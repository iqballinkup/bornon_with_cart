class ProductList {
  final String? pName;
  final int? pQuantity;
  final int? pPrice;
  final String? pImage;
  final String? cImage;
  final String? cName;

  ProductList(
      {this.pName,
      this.pQuantity,
      this.pPrice,
      this.pImage,
      this.cName,
      this.cImage});
}

// List childProducts = [
//   {
//     "pQuantity": 1,
//     "pPrice": 50,
//     "pName": "Lotion",
//     "pImage": "images/child_prod/product1.jpg"
//   },
//   {
//     "pQuantity": 1,
//     "pPrice": 100,
//     "pName": "Baby Dress",
//     "pImage": "images/child_prod/product2.jpg"
//   },
//   {
//     "pQuantity": 1,
//     "pPrice": 150,
//     "pName": "Child Dress",
//     "pImage": "images/child_prod/product3.jpg"
//   },
//   {
//     "pQuantity": 1,
//     "pPrice": 200,
//     "pName": "Shampoo",
//     "pImage": "images/child_prod/product4.jpg"
//   },
//   {
//     "pQuantity": 1,
//     "pPrice": 250,
//     "pName": "Yeallow Dress",
//     "pImage": "images/child_prod/product5.jpg"
//   },
//   {
//     "pQuantity": 1,
//     "pPrice": 300,
//     "pName": "Family Dress",
//     "pImage": "images/child_prod/product6.jpg"
//   }
// ];

// List womenProducts = [
//   {
//     "pQuantity": 1,
//     "pPrice": 50,
//     "pName": "Salwar Kameez",
//     "pImage": "images/women_prod/product1.jpg"
//   },
//   {
//     "pQuantity": 1,
//     "pPrice": 100,
//     "pName": "Saree",
//     "pImage": "images/women_prod/product2.jpg"
//   },
//   {
//     "pQuantity": 1,
//     "pPrice": 150,
//     "pName": "Frock Dress",
//     "pImage": "images/women_prod/product3.jpg"
//   },
//   {
//     "pQuantity": 1,
//     "pPrice": 200,
//     "pName": "Saree",
//     "pImage": "images/women_prod/product4.jpg"
//   },
//   {
//     "pQuantity": 1,
//     "pPrice": 250,
//     "pName": "Saree",
//     "pImage": "images/women_prod/product5.jpg"
//   },
//   {
//     "pQuantity": 1,
//     "pPrice": 300,
//     "pName": "Saree",
//     "pImage": "images/women_prod/product6.jpg"
//   }
// ];

// List menProducts = [
//   {
//     "pQuantity": 1,
//     "pPrice": 50,
//     "pName": "Pant",
//     "pImage": "images/men_prod/product1.jpg"
//   },
//   {
//     "pQuantity": 1,
//     "pPrice": 100,
//     "pName": "Sherwani",
//     "pImage": "images/men_prod/product2.jpg"
//   },
//   {
//     "pQuantity": 1,
//     "pPrice": 150,
//     "pName": "T-Shirt",
//     "pImage": "images/men_prod/product3.jpg"
//   },
//   {
//     "pQuantity": 1,
//     "pPrice": 200,
//     "pName": "Punjabi",
//     "pImage": "images/men_prod/product4.jpg"
//   },
//   {
//     "pQuantity": 1,
//     "pPrice": 250,
//     "pName": "T-Shirt",
//     "pImage": "images/men_prod/product5.jpg"
//   },
//   {
//     "pQuantity": 1,
//     "pPrice": 300,
//     "pName": "Punjabi",
//     "pImage": "images/men_prod/product6.jpg"
//   },
//   {
//     "pQuantity": 1,
//     "pPrice": 350,
//     "pName": "Blazer",
//     "pImage": "images/men_prod/product7.jpeg"
//   },
//   {
//     "pQuantity": 1,
//     "pPrice": 400,
//     "pName": "Shirt",
//     "pImage": "images/men_prod/product8.jpeg"
//   }
// ];

List allProducts = [
  {
    "pQuantity": 1,
    "pPrice": 50,
    "pName": "Lotion",
    "pImage": "images/child_prod/product1.jpg"
  },
  {
    "pQuantity": 1,
    "pPrice": 100,
    "pName": "Baby Dress",
    "pImage": "images/child_prod/product2.jpg"
  },
  {
    "pQuantity": 1,
    "pPrice": 50,
    "pName": "Salwar Kameez",
    "pImage": "images/women_prod/product1.jpg"
  },
  {
    "pQuantity": 1,
    "pPrice": 100,
    "pName": "Saree",
    "pImage": "images/women_prod/product2.jpg"
  },
  {
    "pQuantity": 1,
    "pPrice": 50,
    "pName": "Pant",
    "pImage": "images/men_prod/product1.jpg"
  },
  {
    "pQuantity": 1,
    "pPrice": 100,
    "pName": "Sherwani",
    "pImage": "images/men_prod/product2.jpg"
  },
  {
    "pQuantity": 1,
    "pPrice": 150,
    "pName": "Child Dress",
    "pImage": "images/child_prod/product3.jpg"
  },
  {
    "pQuantity": 1,
    "pPrice": 200,
    "pName": "Shampoo",
    "pImage": "images/child_prod/product4.jpg"
  },
  {
    "pQuantity": 1,
    "pPrice": 250,
    "pName": "Saree",
    "pImage": "images/women_prod/product5.jpg"
  },
  {
    "pQuantity": 1,
    "pPrice": 300,
    "pName": "Saree",
    "pImage": "images/women_prod/product6.jpg"
  },
  {
    "pQuantity": 1,
    "pPrice": 350,
    "pName": "Blazer",
    "pImage": "images/men_prod/product7.jpeg"
  },
  {
    "pQuantity": 1,
    "pPrice": 400,
    "pName": "Shirt",
    "pImage": "images/men_prod/product8.jpeg"
  },
  {
    "pQuantity": 1,
    "pPrice": 150,
    "pName": "T-Shirt",
    "pImage": "images/men_prod/product3.jpg"
  },
  {
    "pQuantity": 1,
    "pPrice": 250,
    "pName": "T-Shirt",
    "pImage": "images/men_prod/product5.jpg"
  },
  {
    "pQuantity": 1,
    "pPrice": 300,
    "pName": "Punjabi",
    "pImage": "images/men_prod/product6.jpg"
  },
  {
    "pQuantity": 1,
    "pPrice": 250,
    "pName": "Yeallow Dress",
    "pImage": "images/child_prod/product5.jpg"
  },
  {
    "pQuantity": 1,
    "pPrice": 300,
    "pName": "Family Dress",
    "pImage": "images/child_prod/product6.jpg"
  },
  {
    "pQuantity": 1,
    "pPrice": 150,
    "pName": "Frock Dress",
    "pImage": "images/women_prod/product3.jpg"
  },
  {
    "pQuantity": 1,
    "pPrice": 200,
    "pName": "Saree",
    "pImage": "images/women_prod/product4.jpg"
  },
  {
    "pQuantity": 1,
    "pPrice": 200,
    "pName": "Punjabi",
    "pImage": "images/men_prod/product4.jpg"
  },
];

// List categoryList = [

//   [
//     {"cName": "kids", "cImage": "images/child_prod/product2.jpg"},
//     {"cName": "Women", "cImage": "images/women_prod/product1.jpg"},
//     {"cName": "Men", "cImage": "images/men_prod/product2.jpg"},
//   ],
//   [
//     {"cName": "kids", "cImage": "images/child_prod/product2.jpg"},
//     {"cName": "Women", "cImage": "images/women_prod/product1.jpg"},
//     {"cName": "Men", "cImage": "images/men_prod/product2.jpg"},
//   ],
//   [
//     {"cName": "kids", "cImage": "images/child_prod/product2.jpg"},
//     {"cName": "Women", "cImage": "images/women_prod/product1.jpg"},
//     {"cName": "Men", "cImage": "images/men_prod/product2.jpg"},
//   ],
// ];

List categoryList = [
  {
    "catItemName": "Kids",
    "catItemImage": "images/child_prod/product1.jpg",
    "pList": [
      {
        "pQuantity": 1,
        "pPrice": 50,
        "pName": "Lotion",
        "pImage": "images/child_prod/product1.jpg"
      },
      {
        "pQuantity": 1,
        "pPrice": 100,
        "pName": "Baby Dress",
        "pImage": "images/child_prod/product2.jpg"
      },
      {
        "pQuantity": 1,
        "pPrice": 150,
        "pName": "Child Dress",
        "pImage": "images/child_prod/product3.jpg"
      },
      {
        "pQuantity": 1,
        "pPrice": 200,
        "pName": "Shampoo",
        "pImage": "images/child_prod/product4.jpg"
      },
      {
        "pQuantity": 1,
        "pPrice": 250,
        "pName": "Yeallow Dress",
        "pImage": "images/child_prod/product5.jpg"
      },
      {
        "pQuantity": 1,
        "pPrice": 300,
        "pName": "Family Dress",
        "pImage": "images/child_prod/product6.jpg"
      }
    ]
  },
  {
    "catItemName": "Women",
    "catItemImage": "images/women_prod/product1.jpg",
    'pList': [
      {
        "pQuantity": 1,
        "pPrice": 50,
        "pName": "Salwar Kameez",
        "pImage": "images/women_prod/product1.jpg"
      },
      {
        "pQuantity": 1,
        "pPrice": 100,
        "pName": "Saree",
        "pImage": "images/women_prod/product2.jpg"
      },
      {
        "pQuantity": 1,
        "pPrice": 150,
        "pName": "Frock Dress",
        "pImage": "images/women_prod/product3.jpg"
      },
      {
        "pQuantity": 1,
        "pPrice": 200,
        "pName": "Saree",
        "pImage": "images/women_prod/product4.jpg"
      },
      {
        "pQuantity": 1,
        "pPrice": 250,
        "pName": "Saree",
        "pImage": "images/women_prod/product5.jpg"
      },
      {
        "pQuantity": 1,
        "pPrice": 300,
        "pName": "Saree",
        "pImage": "images/women_prod/product6.jpg"
      }
    ]
  },
  {
    "catItemName": "Men",
    "catItemImage": "images/men_prod/product1.jpg",
    'pList': [
      {
        "pQuantity": 1,
        "pPrice": 50,
        "pName": "Pant",
        "pImage": "images/men_prod/product1.jpg"
      },
      {
        "pQuantity": 1,
        "pPrice": 100,
        "pName": "Sherwani",
        "pImage": "images/men_prod/product2.jpg"
      },
      {
        "pQuantity": 1,
        "pPrice": 150,
        "pName": "T-Shirt",
        "pImage": "images/men_prod/product3.jpg"
      },
      {
        "pQuantity": 1,
        "pPrice": 200,
        "pName": "Punjabi",
        "pImage": "images/men_prod/product4.jpg"
      },
      {
        "pQuantity": 1,
        "pPrice": 250,
        "pName": "T-Shirt",
        "pImage": "images/men_prod/product5.jpg"
      },
      {
        "pQuantity": 1,
        "pPrice": 300,
        "pName": "Punjabi",
        "pImage": "images/men_prod/product6.jpg"
      },
      {
        "pQuantity": 1,
        "pPrice": 350,
        "pName": "Blazer",
        "pImage": "images/men_prod/product7.jpeg"
      },
      {
        "pQuantity": 1,
        "pPrice": 400,
        "pName": "Shirt",
        "pImage": "images/men_prod/product8.jpeg"
      }
    ]
  },
];
