// To parse this JSON data, do
// final hotDeaProductslModel = hotDeaProductslModelFromJson(jsonString);

import 'dart:convert';

HotDeaProductslModel hotDeaProductslModelFromJson(String str) =>
    HotDeaProductslModel.fromJson(json.decode(str));

String hotDeaProductslModelToJson(HotDeaProductslModel data) =>
    json.encode(data.toJson());

class HotDeaProductslModel {
  HotDeaProductslModel({
    this.data,
  });

  final Data? data;

  factory HotDeaProductslModel.fromJson(Map<String, dynamic> json) =>
      HotDeaProductslModel(
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "data": data?.toJson(),
      };
}

class Data {
  Data({
    this.currentPage,
    this.data,
    this.firstPageUrl,
    this.from,
    this.lastPage,
    this.lastPageUrl,
    this.links,
    this.nextPageUrl,
    this.path,
    this.perPage,
    this.prevPageUrl,
    this.to,
    this.total,
  });

  final int? currentPage;
  final List<Datum>? data;
  final String? firstPageUrl;
  final int? from;
  final int? lastPage;
  final String? lastPageUrl;
  final List<Link>? links;
  final dynamic nextPageUrl;
  final String? path;
  final int? perPage;
  final dynamic prevPageUrl;
  final int? to;
  final int? total;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        currentPage: json["current_page"],
        data: json["data"] == null
            ? []
            : List<Datum>.from(json["data"]!.map((x) => Datum.fromJson(x))),
        firstPageUrl: json["first_page_url"],
        from: json["from"],
        lastPage: json["last_page"],
        lastPageUrl: json["last_page_url"],
        links: json["links"] == null
            ? []
            : List<Link>.from(json["links"]!.map((x) => Link.fromJson(x))),
        nextPageUrl: json["next_page_url"],
        path: json["path"],
        perPage: json["per_page"],
        prevPageUrl: json["prev_page_url"],
        to: json["to"],
        total: json["total"],
      );

  Map<String, dynamic> toJson() => {
        "current_page": currentPage,
        "data": data == null
            ? []
            : List<dynamic>.from(data!.map((x) => x.toJson())),
        "first_page_url": firstPageUrl,
        "from": from,
        "last_page": lastPage,
        "last_page_url": lastPageUrl,
        "links": links == null
            ? []
            : List<dynamic>.from(links!.map((x) => x.toJson())),
        "next_page_url": nextPageUrl,
        "path": path,
        "per_page": perPage,
        "prev_page_url": prevPageUrl,
        "to": to,
        "total": total,
      };
}

class Datum {
  Datum({
    this.id,
    this.productCode,
    this.name,
    this.slug,
    this.model,
    this.price,
    this.simillarPorduct,
    this.similarDiscount,
    this.sizeId,
    this.colorId,
    this.categoryId,
    this.subCategoryId,
    this.brandId,
    this.discount,
    this.shortDetails,
    this.description,
    this.mainImage,
    this.thumbImage,
    this.smallImage,
    this.sizeguide,
    this.isFeature,
    this.isCollectionTitle1,
    this.isCollectionTitle2,
    this.isDeal,
    this.isTailoring,
    this.tailoringCharge,
    this.isTrending,
    this.newArrival,
    this.dealStart,
    this.dealEnd,
    this.rewardPoint,
    this.status,
    this.quantity,
    this.saveBy,
    this.updateBy,
    this.ipAddress,
    this.deletedAt,
    this.createdAt,
    this.updatedAt,
    this.currencyAmount,
    this.category,
    this.productImage,
  });

  final int? id;
  final String? productCode;
  final String? name;
  final String? slug;
  final String? model;
  final int? price;
  final String? simillarPorduct;
  final String? similarDiscount;
  final String? sizeId;
  final String? colorId;
  final int? categoryId;
  final int? subCategoryId;
  final int? brandId;
  final String? discount;
  final String? shortDetails;
  final String? description;
  final String? mainImage;
  final String? thumbImage;
  final String? smallImage;
  final String? sizeguide;
  final String? isFeature;
  final String? isCollectionTitle1;
  final String? isCollectionTitle2;
  final String? isDeal;
  final String? isTailoring;
  final dynamic tailoringCharge;
  final String? isTrending;
  final String? newArrival;
  final dynamic dealStart;
  final dynamic dealEnd;
  final String? rewardPoint;
  final int? status;
  final int? quantity;
  final String? saveBy;
  final dynamic updateBy;
  final String? ipAddress;
  final dynamic deletedAt;
  final DateTime? createdAt;
  final DateTime? updatedAt;
  final String? currencyAmount;
  final Category? category;
  final List<ProductImage>? productImage;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        productCode: json["product_code"],
        name: json["name"],
        slug: json["slug"],
        model: json["model"],
        price: json["price"],
        simillarPorduct: json["simillar_porduct"],
        similarDiscount: json["similar_discount"],
        sizeId: json["size_id"],
        colorId: json["color_id"],
        categoryId: json["category_id"],
        subCategoryId: json["sub_category_id"],
        brandId: json["brand_id"],
        discount: json["discount"],
        shortDetails: json["short_details"],
        description: json["description"],
        mainImage: json["main_image"],
        thumbImage: json["thumb_image"],
        smallImage: json["small_image"],
        sizeguide: json["sizeguide"],
        isFeature: json["is_feature"],
        isCollectionTitle1: json["is_collection_title_1"],
        isCollectionTitle2: json["is_collection_title_2"],
        isDeal: json["is_deal"],
        isTailoring: json["is_tailoring"],
        tailoringCharge: json["tailoring_charge"],
        isTrending: json["is_trending"],
        newArrival: json["new_arrival"],
        dealStart: json["deal_start"],
        dealEnd: json["deal_end"],
        rewardPoint: json["reward_point"],
        status: json["status"],
        quantity: json["quantity"],
        saveBy: json["save_by"],
        updateBy: json["update_by"],
        ipAddress: json["ip_address"],
        deletedAt: json["deleted_at"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        currencyAmount: json["currency_amount"],
        category: json["category"] == null
            ? null
            : Category.fromJson(json["category"]),
        productImage: json["product_image"] == null
            ? []
            : List<ProductImage>.from(
                json["product_image"]!.map((x) => ProductImage.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "product_code": productCode,
        "name": name,
        "slug": slug,
        "model": model,
        "price": price,
        "simillar_porduct": simillarPorduct,
        "similar_discount": similarDiscount,
        "size_id": sizeId,
        "color_id": colorId,
        "category_id": categoryId,
        "sub_category_id": subCategoryId,
        "brand_id": brandId,
        "discount": discount,
        "short_details": shortDetails,
        "description": description,
        "main_image": mainImage,
        "thumb_image": thumbImage,
        "small_image": smallImage,
        "sizeguide": sizeguide,
        "is_feature": isFeature,
        "is_collection_title_1": isCollectionTitle1,
        "is_collection_title_2": isCollectionTitle2,
        "is_deal": isDeal,
        "is_tailoring": isTailoring,
        "tailoring_charge": tailoringCharge,
        "is_trending": isTrending,
        "new_arrival": newArrival,
        "deal_start": dealStart,
        "deal_end": dealEnd,
        "reward_point": rewardPoint,
        "status": status,
        "quantity": quantity,
        "save_by": saveBy,
        "update_by": updateBy,
        "ip_address": ipAddress,
        "deleted_at": deletedAt,
        "created_at": createdAt?.toIso8601String(),
        "updated_at": updatedAt?.toIso8601String(),
        "currency_amount": currencyAmount,
        "category": category?.toJson(),
        "product_image": productImage == null
            ? []
            : List<dynamic>.from(productImage!.map((x) => x.toJson())),
      };
}

class Category {
  Category({
    this.id,
    this.name,
    this.slug,
    this.details,
    this.image,
    this.thumbimage,
    this.smallimage,
    this.status,
    this.isHomepage,
    this.isMenu,
    this.saveBy,
    this.updatedBy,
    this.ipAddress,
    this.deletedAt,
    this.createdAt,
    this.updatedAt,
  });

  final int? id;
  final String? name;
  final String? slug;
  final String? details;
  final String? image;
  final String? thumbimage;
  final String? smallimage;
  final String? status;
  final String? isHomepage;
  final String? isMenu;
  final String? saveBy;
  final dynamic updatedBy;
  final String? ipAddress;
  final dynamic deletedAt;
  final DateTime? createdAt;
  final DateTime? updatedAt;

  factory Category.fromJson(Map<String, dynamic> json) => Category(
        id: json["id"],
        name: json["name"],
        slug: json["slug"],
        details: json["details"],
        image: json["image"],
        thumbimage: json["thumbimage"],
        smallimage: json["smallimage"],
        status: json["status"],
        isHomepage: json["is_homepage"],
        isMenu: json["is_menu"],
        saveBy: json["save_by"],
        updatedBy: json["updated_by"],
        ipAddress: json["ip_address"],
        deletedAt: json["deleted_at"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "slug": slug,
        "details": details,
        "image": image,
        "thumbimage": thumbimage,
        "smallimage": smallimage,
        "status": status,
        "is_homepage": isHomepage,
        "is_menu": isMenu,
        "save_by": saveBy,
        "updated_by": updatedBy,
        "ip_address": ipAddress,
        "deleted_at": deletedAt,
        "created_at": createdAt?.toIso8601String(),
        "updated_at": updatedAt?.toIso8601String(),
      };
}

class ProductImage {
  ProductImage({
    this.id,
    this.productId,
    this.otherMainImage,
    this.otherMediamImage,
    this.otherSmallImage,
    this.createdAt,
    this.updatedAt,
  });

  final int? id;
  final int? productId;
  final String? otherMainImage;
  final String? otherMediamImage;
  final String? otherSmallImage;
  final DateTime? createdAt;
  final DateTime? updatedAt;

  factory ProductImage.fromJson(Map<String, dynamic> json) => ProductImage(
        id: json["id"],
        productId: json["product_id"],
        otherMainImage: json["other_main_image"],
        otherMediamImage: json["other_mediam_image"],
        otherSmallImage: json["other_small_image"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "product_id": productId,
        "other_main_image": otherMainImage,
        "other_mediam_image": otherMediamImage,
        "other_small_image": otherSmallImage,
        "created_at": createdAt?.toIso8601String(),
        "updated_at": updatedAt?.toIso8601String(),
      };
}

class Link {
  Link({
    this.url,
    this.label,
    this.active,
  });

  final String? url;
  final String? label;
  final bool? active;

  factory Link.fromJson(Map<String, dynamic> json) => Link(
        url: json["url"],
        label: json["label"],
        active: json["active"],
      );

  Map<String, dynamic> toJson() => {
        "url": url,
        "label": label,
        "active": active,
      };
}
