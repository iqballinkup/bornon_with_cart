import 'package:flutter/material.dart';

class CustomTextStyle {
  static const TextStyle n_s16_cb = TextStyle(
    fontSize: 16,
    color: Colors.black,
  );
  static const TextStyle b_s16_cb = TextStyle(
    fontSize: 16,
    color: Colors.black,
    fontWeight: FontWeight.bold,
  );
  static const TextStyle n_s14_cb = TextStyle(
    fontSize: 14,
    color: Colors.black,
  );
  static const TextStyle b_s14_cb = TextStyle(
    fontSize: 14,
    color: Colors.black,
    fontWeight: FontWeight.bold,
  );
}
