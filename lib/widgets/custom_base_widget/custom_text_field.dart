import 'package:flutter/material.dart';

class MyCustomTextFormField {
  TextFormField getCustomEditTextArea(
      {String? hintValue = "",
      bool? validation,
      double? h2TextSize,
      TextEditingController? controller,
      TextInputType keyboardType = TextInputType.text,
      TextStyle? textStyle,
      String? validationErrorMsg}) {
    TextFormField textFormField = TextFormField(
      keyboardType: keyboardType,
      style: textStyle,
      controller: controller,
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'invalid';
        }
        return null;
      },
      decoration: InputDecoration(
        hintText: hintValue,
        hintStyle: TextStyle(fontSize: h2TextSize),
        errorStyle: const TextStyle(fontSize: 0.01),
        contentPadding: EdgeInsets.only(left: 10.0, top: 0.0, bottom: 0.0, right: 0.0),
      ),
    );
    return textFormField;
  }
}
