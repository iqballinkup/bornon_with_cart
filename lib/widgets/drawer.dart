import 'package:cart_and_hive/page_view_page.dart';
import 'package:cart_and_hive/pages/signup_login/login_page.dart';
import 'package:cart_and_hive/pages/user/change_mobile_page.dart';
import 'package:cart_and_hive/pages/user/change_password_page.dart';
import 'package:cart_and_hive/pages/user/my_order.dart';
import 'package:cart_and_hive/pages/user/update_profile_page.dart';
import 'package:flutter/material.dart';

import '../pages/user/user_dashboard.dart';

class MyCustomDrawer extends StatefulWidget {
  const MyCustomDrawer({super.key});

  @override
  State<MyCustomDrawer> createState() => _MyCustomDrawerState();
}

class _MyCustomDrawerState extends State<MyCustomDrawer> {
  @override
  Widget build(BuildContext context) {
    var screenWidth = MediaQuery.of(context).size.width;
    return Drawer(
      backgroundColor: Colors.grey[200],
      width: screenWidth - 100,
      child: ListView(
        children: [
          SizedBox(
            height: 100,
            child: DrawerHeader(
                decoration: BoxDecoration(
                  color: Colors.white,
                ),
                child: Row(
                  children: [
                    CircleAvatar(
                        radius: 30,
                        backgroundColor: Colors.grey,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(50),
                          child: Image.asset(
                            "images/men_prod/product4.jpg",
                            fit: BoxFit.cover,
                          ),
                        )),
                    SizedBox(
                      width: 10,
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Iqbal Riaz"),
                        Text("0123456789"),
                      ],
                    )
                  ],
                )),
          ),
          Container(
            color: Colors.white,
            margin: EdgeInsets.only(bottom: 8),
            height: 35,
            child: ListTile(
              leading: Icon(Icons.home),
              title: Text("Home"),
              trailing: Icon(
                Icons.arrow_forward_ios,
                size: 15,
              ),
              dense: true,
              visualDensity: VisualDensity(vertical: -4),
              onTap: () {
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(
                      builder: (context) => PageViewPage(),
                    ),
                    (route) => false);
              },
            ),
          ),
          Container(
            color: Colors.white,
            margin: EdgeInsets.only(bottom: 8),
            height: 35,
            child: ListTile(
              leading: Icon(Icons.person),
              title: Text("My Dashboard"),
              trailing: Icon(
                Icons.arrow_forward_ios,
                size: 15,
              ),
              dense: true,
              visualDensity: VisualDensity(vertical: -4),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => UserDashboard(),
                    )).then((value) {
                  setState(() {});
                });
              },
            ),
          ),
          Container(
            color: Colors.white,
            margin: EdgeInsets.only(bottom: 8),
            height: 35,
            child: ListTile(
              leading: Icon(Icons.update),
              title: Text("Update Profile"),
              trailing: Icon(
                Icons.arrow_forward_ios,
                size: 15,
              ),
              dense: true,
              visualDensity: VisualDensity(vertical: -4),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => UpdateProfile(),
                    )).then((value) {
                  setState(() {});
                });
              },
            ),
          ),
          Container(
            color: Colors.white,
            margin: EdgeInsets.only(bottom: 8),
            height: 35,
            child: ListTile(
              leading: Icon(Icons.lock),
              title: Text("Change Password"),
              trailing: Icon(
                Icons.arrow_forward_ios,
                size: 15,
              ),
              dense: true,
              visualDensity: VisualDensity(vertical: -4),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ChangePassword(),
                    )).then((value) {
                  setState(() {});
                });
              },
            ),
          ),
          Container(
            color: Colors.white,
            margin: EdgeInsets.only(bottom: 8),
            height: 35,
            child: ListTile(
              leading: Icon(Icons.phone),
              title: Text("Change Mobile"),
              trailing: Icon(
                Icons.arrow_forward_ios,
                size: 15,
              ),
              dense: true,
              visualDensity: VisualDensity(vertical: -4),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ChangeMobile(),
                    )).then((value) {
                  setState(() {});
                });
              },
            ),
          ),
          Container(
            color: Colors.white,
            margin: EdgeInsets.only(bottom: 8),
            height: 35,
            child: ListTile(
              leading: Icon(Icons.shopping_cart),
              title: Text("My Order"),
              trailing: Icon(
                Icons.arrow_forward_ios,
                size: 15,
              ),
              dense: true,
              visualDensity: VisualDensity(vertical: -4),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => MyOrder(),
                    )).then((value) {
                  setState(() {});
                });
              },
            ),
          ),
          Container(
            color: Colors.white,
            margin: EdgeInsets.only(bottom: 8),
            height: 35,
            child: ListTile(
              leading: Icon(Icons.logout),
              title: Text("Logout"),
              trailing: Icon(
                Icons.arrow_forward_ios,
                size: 15,
              ),
              dense: true,
              visualDensity: VisualDensity(vertical: -4),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => LoginPage(),
                    ));
              },
            ),
          ),
        ],
      ),
    );
  }
}
