import 'package:flutter/material.dart';
import 'package:badges/badges.dart' as badges;
import 'package:hive/hive.dart';

import '../pages/cart_screen.dart';

class ShoppingCartBadge extends StatefulWidget {
  const ShoppingCartBadge({Key? key}) : super(key: key);

  @override
  State<ShoppingCartBadge> createState() => _ShoppingCartBadgeState();
}

class _ShoppingCartBadgeState extends State<ShoppingCartBadge> {
  int _cartBadgeAmount = 1;

  late final Box box;

  // Add info to people box

  @override
  void initState() {
    super.initState();

    // Get reference to an already opened box
    box = Hive.box('productBox');
  }

  @override
  Widget build(BuildContext context) {
    return badges.Badge(
      position: badges.BadgePosition.topEnd(top: 0, end: 3),
      badgeAnimation: badges.BadgeAnimation.slide(
          // disappearanceFadeAnimationDuration: Duration(milliseconds: 200),
          // curve: Curves.easeInCubic,
          ),
      showBadge: true,
      badgeStyle: badges.BadgeStyle(
        badgeColor: Colors.red,
      ),
      badgeContent: Text(
        box.length.toString(),
        style: TextStyle(color: Colors.white, fontSize: 8),
      ),
      child: IconButton(
          icon: Icon(Icons.shopping_cart),
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => CartScreen(),
                )).then((value) {
              setState(() {});
            });
          }),
    );
  }
}
