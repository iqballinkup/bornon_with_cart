import 'package:flutter/material.dart';

class CustomPageView extends StatefulWidget {
  const CustomPageView({super.key});

  @override
  _CustomPageViewState createState() => _CustomPageViewState();
}

class _CustomPageViewState extends State<CustomPageView> {
  int _selectedIndex = 0;
  final PageController _controller = PageController();


  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Color(0xffD4EAFF),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            setState(() {
              _selectedIndex = 0;
              
              if (_controller.hasClients) {
                _controller.jumpToPage(_selectedIndex);
              }
            });
            print("Home button is tapped");
          },
          backgroundColor: Color.fromARGB(255, 111, 149, 169),
          child: Icon(Icons.home),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        body: Column(
          children: [
            Expanded(
              child: PageView(
                physics: const NeverScrollableScrollPhysics(),
                controller: _controller,
                children: [
                  //HomePage(),
                //Page2(),
                //Page3(),
                 //Page4(),
                ],
              ),
            ),
            Container(
              height: 55,
              color: Color.fromARGB(255, 87, 117, 133),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  InkWell(
                    onTap: () {
                      setState(() {
                        _selectedIndex = 1;
                      });
                      if (_controller.hasClients) {
                        _controller.jumpToPage(_selectedIndex);
                      }
                      print("Product button is tapped");
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.production_quantity_limits,
                          color:
                              _selectedIndex == 1 ? Colors.black : Colors.white,
                          size: 23,
                        ),
                        Text(
                          "Product",
                          style: TextStyle(
                            color: _selectedIndex == 1
                                ? Colors.black
                                : Colors.white,
                          ),
                        )
                      ],
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        _selectedIndex = 2;
                        if (_controller.hasClients) {
                          _controller.jumpToPage(_selectedIndex);
                        }
                      });
                      print("Category button is tapped");
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.category,
                          color:
                              _selectedIndex == 2 ? Colors.black : Colors.white,
                          size: 23,
                        ),
                        Text(
                          "Category",
                          style: TextStyle(
                            color: _selectedIndex == 2
                                ? Colors.black
                                : Colors.white,
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    width: 40.0,
                  ),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        _selectedIndex = 3;
                        if (_controller.hasClients) {
                          _controller.jumpToPage(_selectedIndex);
                        }
                      });
                      print("Cart button is tapped");
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.shopping_cart,
                          color:
                              _selectedIndex == 3 ? Colors.black : Colors.white,
                          size: 23,
                        ),
                        Text(
                          "Cart",
                          style: TextStyle(
                            color: _selectedIndex == 3
                                ? Colors.black
                                : Colors.white,
                          ),
                        )
                      ],
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        _selectedIndex = 4;
                        if (_controller.hasClients) {
                          _controller.jumpToPage(_selectedIndex);
                        }
                      });
                      print("Account button is tapped");
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.person,
                          color:
                              _selectedIndex == 4 ? Colors.black : Colors.white,
                          size: 23,
                        ),
                        Text(
                          "Account",
                          style: TextStyle(
                            color: _selectedIndex == 4
                                ? Colors.black
                                : Colors.white,
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
