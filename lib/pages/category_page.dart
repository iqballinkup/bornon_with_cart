import 'package:cart_and_hive/model/product_list.dart';
import 'package:cart_and_hive/pages/category_details_page.dart';
import 'package:cart_and_hive/widgets/drawer.dart';
import 'package:cart_and_hive/widgets/shoping_cart_badge.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';

class CategoryPage extends StatefulWidget {
  const CategoryPage({super.key});

  @override
  State<CategoryPage> createState() => _CategoryPageState();
}

class _CategoryPageState extends State<CategoryPage> {
  late final Box box;

  @override
  void initState() {
    super.initState();
    // Get reference to an already opened box
    box = Hive.box('productBox');
  }

  double productTextSize = 14.0;
  double productPriceSize = 14.0;

  @override
  Widget build(BuildContext context) {
    var screenHeight = MediaQuery.of(context).size.height;
    var screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 45,
        backgroundColor: Color.fromARGB(255, 87, 117, 133),
        title: Text("Category"),
        actions: [
          Padding(
            padding: const EdgeInsets.only(top: 5.0, bottom: 5.0, right: 10.0),
            child: ShoppingCartBadge(),
          ),
        ],
      ),
      drawer: MyCustomDrawer(),
      body: Padding(
        padding: const EdgeInsets.only(
            top: 5.0, left: 10.0, right: 10.0, bottom: 5.0),
        child: Column(
          children: [
            SingleChildScrollView(
              child: Container(
                height: screenHeight - 150,
                width: screenWidth,
                child: GridView.builder(
                  itemCount: categoryList.length,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisSpacing: 5,
                      mainAxisSpacing: 10,
                      crossAxisCount: MediaQuery.of(context).orientation ==
                              Orientation.landscape
                          ? 4
                          : 2,
                      mainAxisExtent: 250),
                  itemBuilder: (context, index) {
                    return InkWell(
                      onTap: () {
                        Navigator.of(context).push(
                            MaterialPageRoute(builder: (BuildContext context) {
                          return CategoryDetailsPage(
                            // categoryName: categoryItem[index]["catItemName"],
                            categoryName: categoryList[index]["catItemName"],
                            categoryIndex: index,
                          );
                        })).then((_) => setState(() {}));
                      },
                      child: Card(
                          elevation: 8,
                          child: Stack(
                            children: [
                              Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(top: 10.0),
                                    height: 200,
                                    child: Image.asset(
                                      categoryList[index]["catItemImage"],
                                      // categoryList[0]["kidsList"][index]
                                      //     ['pImage']
                                    ),
                                  ),
                                  Container(
                                    alignment: Alignment.centerLeft,
                                    margin: EdgeInsets.only(left: 10),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          categoryList[index]["catItemName"],
                                          style: TextStyle(
                                              fontSize: productTextSize),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              Positioned(
                                  right: 10,
                                  top: 10,
                                  child: Container(
                                      height: 30,
                                      width: 30,
                                      decoration: BoxDecoration(
                                          color: Colors.redAccent,
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(20))),
                                      child: Align(
                                          alignment: Alignment.center,
                                          child: Text(
                                            "25%",
                                            style: TextStyle(
                                                fontSize: 13,
                                                color: Colors.white),
                                          ))))
                            ],
                          )),
                    );
                  },
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
