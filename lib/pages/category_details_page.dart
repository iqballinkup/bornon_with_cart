import 'package:cart_and_hive/model/product_list.dart';
import 'package:cart_and_hive/pages/category_product_details.dart';
import 'package:cart_and_hive/pages/product_details_page.dart';
import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import '../widgets/shoping_cart_badge.dart';

class CategoryDetailsPage extends StatefulWidget {
  final String categoryName;
  // final String scImage, scName;
  // int scPrice, sQuantity;
  int categoryIndex;
  CategoryDetailsPage({
    super.key,
    required this.categoryName,
    // required this.sQuantity,
    // required this.scImage,
    // required this.scName,
    // required this.scPrice,
    required this.categoryIndex,
  });

  @override
  State<CategoryDetailsPage> createState() => _CategoryDetailsPageState();
}

class _CategoryDetailsPageState extends State<CategoryDetailsPage> {
  late final Box box;

  @override
  void initState() {
    setState(() {});
    super.initState();
    print(widget.categoryName);
    // print(widget.scImage);
    // print(widget.sQuantity);
    // print(widget.scName);
    // print(widget.scPrice);
    print(widget.categoryIndex);
    print("length: ${categoryList[widget.categoryIndex]["pList"].length}");
    // Get reference to an already opened box
    box = Hive.box('productBox');
  }

  double productTextSize = 14.0;
  double productPriceSize = 14.0;

  @override
  Widget build(BuildContext context) {
    var screenHeight = MediaQuery.of(context).size.height;
    var screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: Color.fromARGB(255, 226, 226, 226),
      appBar: AppBar(
        toolbarHeight: 45,
        backgroundColor: Color.fromARGB(255, 87, 117, 133),
        title: Text(widget.categoryName.toString()),
        // title: Text("Sub-Categories"),
        actions: [
          Padding(
            padding: const EdgeInsets.only(top: 5.0, bottom: 5.0, right: 10.0),
            child: ShoppingCartBadge(),
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.only(
            top: 5.0, left: 10.0, right: 10.0, bottom: 5.0),
        child: Column(
          children: [
            Expanded(
              // height: screenHeight - 100,
              // width: screenWidth,
              child: GridView.builder(
                itemCount: categoryList[widget.categoryIndex]["pList"].length,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisSpacing: 5,
                    mainAxisSpacing: 10,
                    crossAxisCount: MediaQuery.of(context).orientation ==
                            Orientation.landscape
                        ? 4
                        : 2,
                    mainAxisExtent: 250),
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: () {
                      Navigator.of(context).push(
                          MaterialPageRoute(builder: (BuildContext context) {
                        // return ProductDetailsPage(
                        //   pImage:  categoryList[widget.categoryIndex]
                        //                     ["pList"][index]["pImage"],
                        //   pName: categoryList[widget.categoryIndex]
                        //                     ["pList"][index]["pName"],
                        //   pPrice: categoryList[widget.categoryIndex]
                        //                     ["pList"][index]["pPrice"],
                        //   pQuantity: categoryList[widget.categoryIndex]
                        //                     ["pList"][index]["pQuantity"],
                        // );
                        return CategoryProductDetails(
                          catProPrice: categoryList[widget.categoryIndex]
                              ["pList"][index]["pPrice"],
                          catProName: categoryList[widget.categoryIndex]
                              ["pList"][index]["pName"],
                          catProImage: categoryList[widget.categoryIndex]
                              ["pList"][index]["pImage"],
                          catProQuantity: categoryList[widget.categoryIndex]
                              ["pList"][index]["pQuantity"],
                          catProIndex: index,
                        );
                      })).then((_) => setState(() {}));
                    },
                    child: Card(
                        elevation: 8,
                        child: Stack(
                          children: [
                            Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                  margin: EdgeInsets.only(top: 10.0),
                                  height: 200,
                                  child: Image.asset(
                                    categoryList[widget.categoryIndex]["pList"]
                                        [index]["pImage"],
                                  ),
                                ),
                                Container(
                                  alignment: Alignment.centerLeft,
                                  margin: EdgeInsets.only(left: 10),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        categoryList[widget.categoryIndex]
                                            ["pList"][index]["pName"],
                                        style: TextStyle(
                                            fontSize: productTextSize),
                                      ),
                                      Text(
                                        r'$' +
                                            categoryList[widget.categoryIndex]
                                                    ["pList"][index]["pPrice"]
                                                .toString(),
                                        style: TextStyle(
                                            fontSize: productTextSize,
                                            color: Colors.blue,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            Positioned(
                                right: 10,
                                top: 10,
                                child: Container(
                                    height: 30,
                                    width: 30,
                                    decoration: BoxDecoration(
                                        color: Colors.redAccent,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(20))),
                                    child: Align(
                                        alignment: Alignment.center,
                                        child: Text(
                                          "25%",
                                          style: TextStyle(
                                              fontSize: 13,
                                              color: Colors.white),
                                        ))))
                          ],
                        )),
                  );
                },
              ),
            )
          ],
        ),
      ),

    );
  }
}
