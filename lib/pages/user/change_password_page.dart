import 'package:flutter/material.dart';

class ChangePassword extends StatefulWidget {
  const ChangePassword({super.key});

  @override
  State<ChangePassword> createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  double textFormFieldHeight = 40.0;
  double textFontSize = 16.0;

  bool _obscureText1 = true;
  bool _obscureText2 = true;
  bool _obscureText3 = true;

  TextEditingController oldPassController = TextEditingController();
  TextEditingController newPassController = TextEditingController();
  TextEditingController confirmPassController = TextEditingController();

  void _toggle1() {
    setState(() {
      _obscureText1 = !_obscureText1;
    });
  }

  void _toggle2() {
    setState(() {
      _obscureText2 = !_obscureText2;
    });
  }

  void _toggle3() {
    setState(() {
      _obscureText3 = !_obscureText3;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 45,
        backgroundColor: Color.fromARGB(255, 87, 117, 133),
        title: const Text('Change Password'),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Old Password",
                style: TextStyle(
                    color: Colors.black54,
                    fontSize: textFontSize,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: textFormFieldHeight,
                child: Align(
                  alignment: Alignment.center,
                  child: TextFormField(
                    controller: oldPassController,
                    obscureText: _obscureText1,
                    decoration: InputDecoration(
                        filled: true,
                        contentPadding: EdgeInsets.only(
                            left: 10.0, top: 0.0, bottom: 0.0, right: 0.0),
                        fillColor: Colors.white,
                        border: OutlineInputBorder(),
                        suffixIcon: IconButton(
                          icon: Icon(_obscureText1
                              ? Icons.visibility_off
                              : Icons.visibility),
                          onPressed: () {
                            _toggle1();
                          },
                        )),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return null;
                      }
                      return null;
                    },
                  ),
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              Text(
                "New Password",
                style: TextStyle(
                    color: Colors.black54,
                    fontSize: textFontSize,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: textFormFieldHeight,
                child: TextFormField(
                  controller: newPassController,
                  obscureText: _obscureText2,
                  decoration: InputDecoration(
                      filled: true,
                      contentPadding: EdgeInsets.only(
                          left: 10.0, top: 0.0, bottom: 0.0, right: 0.0),
                      fillColor: Colors.white,
                      border: OutlineInputBorder(),
                      suffixIcon: IconButton(
                        icon: Icon(_obscureText2
                            ? Icons.visibility_off
                            : Icons.visibility),
                        onPressed: () {
                          _toggle2();
                        },
                      )),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return null;
                    }
                    return null;
                  },
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              Text(
                "Confirm Password",
                style: TextStyle(
                    color: Colors.black54,
                    fontSize: textFontSize,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: textFormFieldHeight,
                child: Align(
                  alignment: Alignment.center,
                  child: TextFormField(
                    controller: confirmPassController,
                    obscureText: _obscureText3,
                    decoration: InputDecoration(
                        filled: true,
                        contentPadding: EdgeInsets.only(
                            left: 10.0, top: 0.0, bottom: 0.0, right: 0.0),
                        fillColor: Colors.white,
                        border: OutlineInputBorder(),
                        suffixIcon: IconButton(
                          icon: Icon(_obscureText3
                              ? Icons.visibility_off
                              : Icons.visibility),
                          onPressed: () {
                            _toggle3();
                          },
                        )),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return null;
                      }
                      return null;
                    },
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Align(
                  alignment: Alignment.centerRight,
                  child:
                      ElevatedButton(onPressed: () {}, child: Text("Update")))
            ],
          ),
        ),
      ),
    );
  }
}
