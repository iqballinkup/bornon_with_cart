import 'package:flutter/material.dart';
import 'package:jiffy/jiffy.dart';

class MyOrder extends StatefulWidget {
  const MyOrder({super.key});

  @override
  State<MyOrder> createState() => _MyCouponState();
}

class _MyCouponState extends State<MyOrder> {
  String? firstPickedDate;
  String? secondPickedDate;

  double textFontSize = 16.0;

  List<Map<String, dynamic>> orderList = [
    {"Invoice number": 12345, "Status": 1, "Order date": 12345, "Amount": 200},
    {"Invoice number": 12345, "Status": 1, "Order date": 12345, "Amount": 200},
    {"Invoice number": 12345, "Status": 1, "Order date": 12345, "Amount": 200},
    {"Invoice number": 12345, "Status": 1, "Order date": 12345, "Amount": 200},
    {"Invoice number": 12345, "Status": 1, "Order date": 12345, "Amount": 200},
    {"Invoice number": 12345, "Status": 1, "Order date": 12345, "Amount": 200},
    {"Invoice number": 12345, "Status": 1, "Order date": 12345, "Amount": 200},
    {"Invoice number": 12345, "Status": 1, "Order date": 12345, "Amount": 200},
    {"Invoice number": 12345, "Status": 1, "Order date": 12345, "Amount": 200},
    {"Invoice number": 12345, "Status": 1, "Order date": 12345, "Amount": 200},
    {"Invoice number": 12345, "Status": 1, "Order date": 12345, "Amount": 200},
    {"Invoice number": 12345, "Status": 1, "Order date": 12345, "Amount": 200},
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 45,
        backgroundColor: Color.fromARGB(255, 87, 117, 133),
        title: const Text('My Order'),
      ),
      body: Column(
        children: [
          SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 45,
                    child: GestureDetector(
                      onTap: (() {
                        _selectedDate();
                      }),
                      child: TextFormField(
                        enabled: false,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          suffixIcon: Icon(
                            Icons.calendar_month,
                            color: Colors.black87,
                          ),
                          border:
                              OutlineInputBorder(borderSide: BorderSide.none),
                          hintText: firstPickedDate == null
                              ? Jiffy(DateTime.now()).format("dd - MMM - yyyy")
                              : firstPickedDate,
                          hintStyle:
                              TextStyle(fontSize: 14, color: Colors.black87),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return null;
                          }
                          return null;
                        },
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Center(
                    child: Text(
                      "To",
                      style: TextStyle(
                          color: Colors.black54,
                          fontSize: textFontSize,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  SizedBox(
                    height: 45,
                    child: GestureDetector(
                      onTap: (() {
                        _selectedDate2();
                      }),
                      child: TextFormField(
                        enabled: false,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          suffixIcon: Icon(
                            Icons.calendar_month,
                            color: Colors.black87,
                          ),
                          border:
                              OutlineInputBorder(borderSide: BorderSide.none),
                          hintText: secondPickedDate == null
                              ? Jiffy(DateTime.now()).format("dd - MMM - yyyy")
                              : secondPickedDate,
                          hintStyle:
                              TextStyle(fontSize: 14, color: Colors.black87),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return null;
                          }
                          return null;
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            child: ListView.builder(
              itemCount: orderList.length,
              itemBuilder: (BuildContext context, int index) {
                return Card(
                  elevation: 8,
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Table(
                            border: TableBorder.all(
                                color: Colors.black54, width: 1.5),
                            // columnWidths: {
                            //   0: FlexColumnWidth(3),
                            //   1: FlexColumnWidth(5),
                            // },
                            children: [
                              TableRow(children: [
                                Padding(
                                  padding: const EdgeInsets.all(5.0),
                                  child: Text(
                                    "Invoice number",
                                    style: TextStyle(
                                        color: Colors.black54,
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(5.0),
                                  child: Text(
                                    "${orderList[index]["Invoice number"]}",
                                    style: TextStyle(
                                      color: Colors.black54,
                                      fontSize: textFontSize,
                                    ),
                                  ),
                                ),
                              ]),
                              TableRow(children: [
                                Padding(
                                  padding: const EdgeInsets.all(5.0),
                                  child: Text(
                                    "Status",
                                    style: TextStyle(
                                        color: Colors.black54,
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(5.0),
                                  child: Text(
                                    "${orderList[index]["Status"]}",
                                    style: TextStyle(
                                      color: Colors.black54,
                                      fontSize: textFontSize,
                                    ),
                                  ),
                                ),
                              ]),
                              TableRow(children: [
                                Padding(
                                  padding: const EdgeInsets.all(5.0),
                                  child: Text(
                                    "Order date",
                                    style: TextStyle(
                                        color: Colors.black54,
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(5.0),
                                  child: Text(
                                    "${orderList[index]["Order date"]}",
                                    style: TextStyle(
                                      color: Colors.black54,
                                      fontSize: textFontSize,
                                    ),
                                  ),
                                ),
                              ]),
                              TableRow(children: [
                                Padding(
                                  padding: const EdgeInsets.all(5.0),
                                  child: Text(
                                    "Amount",
                                    style: TextStyle(
                                        color: Colors.black54,
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(5.0),
                                  child: Text(
                                    "${orderList[index]["Amount"]}",
                                    style: TextStyle(
                                      color: Colors.black54,
                                      fontSize: textFontSize,
                                    ),
                                  ),
                                ),
                              ]),
                            ])
                      ],
                    ),
                  ),
                );
              },
            ),
          )
        ],
      ),
    );
  }

  _selectedDate() async {
    final selectedDate = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(1950),
        lastDate: DateTime(2050));
    if (selectedDate != null) {
      setState(() {
        firstPickedDate = Jiffy(selectedDate).format("dd - MMM - yyyy");
      });
    }
  }

  _selectedDate2() async {
    final selectedDate = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(1950),
        lastDate: DateTime(2050));
    if (selectedDate != null) {
      setState(() {
        secondPickedDate = Jiffy(selectedDate).format("dd - MMM - yyyy");
      });
    }
  }
}
