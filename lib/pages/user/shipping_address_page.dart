import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';

class ShippingAddressPage extends StatefulWidget {
  const ShippingAddressPage({super.key});

  @override
  State<ShippingAddressPage> createState() => _ShippingAddressPageState();
}

class _ShippingAddressPageState extends State<ShippingAddressPage> {
  final _formKey = GlobalKey<FormState>();

  TextEditingController _billerNameController = TextEditingController();
  TextEditingController _billerMobileNoController = TextEditingController();
  TextEditingController _billerAddressController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        toolbarHeight: 45,
        backgroundColor: Color.fromARGB(255, 87, 117, 133),
        title: Text('Add Shipping Address'),
      ),
      body: Form(
        key: _formKey,
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                padding: const EdgeInsets.all(8.0),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10)),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Expanded(flex: 1, child: Text("Full Name")),
                        SizedBox(
                          width: 5,
                        ),
                        Expanded(
                          flex: 3,
                          child: TextFormField(
                            controller: _billerNameController,
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'invalid';
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                              hintText: "name",
                              errorStyle: const TextStyle(fontSize: 0.01),
                              contentPadding: EdgeInsets.only(
                                  left: 10.0,
                                  top: 0.0,
                                  bottom: 0.0,
                                  right: 0.0),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(flex: 1, child: Text("Mobile No.")),
                        SizedBox(
                          width: 5,
                        ),
                        Expanded(
                          flex: 3,
                          child: TextFormField(
                            controller: _billerMobileNoController,
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return "invalid";
                              }
                              return null;
                            },
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                              errorStyle: TextStyle(fontSize: 0.01),
                              hintText: "mobile",
                              contentPadding: EdgeInsets.only(
                                  left: 10.0,
                                  top: 0.0,
                                  bottom: 0.0,
                                  right: 0.0),
                            ),
                          ),
                        )
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(flex: 1, child: Text("Address")),
                        SizedBox(
                          width: 5,
                        ),
                        Expanded(
                          flex: 3,
                          child: TextFormField(
                            controller: _billerAddressController,
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return "invalid";
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                              hintText: "address",
                              errorStyle: TextStyle(fontSize: 0.01),
                              contentPadding: EdgeInsets.only(
                                  left: 10.0,
                                  top: 0.0,
                                  bottom: 0.0,
                                  right: 0.0),
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Align(
                        alignment: Alignment.centerRight,
                        child: ElevatedButton(
                            onPressed: () {
                              if (_formKey.currentState!.validate()) {}
                            },
                            child: Text("Update")))
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
