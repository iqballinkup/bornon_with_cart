import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class UpdateProfile extends StatefulWidget {
  const UpdateProfile({super.key});

  @override
  State<UpdateProfile> createState() => _UpdateProfileState();
}

class _UpdateProfileState extends State<UpdateProfile> {
  TextEditingController IDController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController addressController = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: Scaffold(
        appBar: AppBar(
          toolbarHeight: 45,
          backgroundColor: Color.fromARGB(255, 87, 117, 133),
          title: const Text('Update Profile'),
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Center(
                  child: Container(
                    height: 150,
                    width: 150,
                    child: Stack(
                      clipBehavior: Clip.none,
                      fit: StackFit.expand,
                      children: [
                        imageFile == null
                            ? Container(
                                child: const CircleAvatar(
                                  backgroundImage: NetworkImage(
                                      'https://st.depositphotos.com/2218212/2938/i/450/depositphotos_29387653-stock-photo-facebook-profile.jpg'),
                                ),
                              )
                            : Container(
                                height: 100,
                                width: 100,
                                child: ClipOval(
                                  child: Image.file(
                                    File(imageFile!.path),
                                    fit: BoxFit.fill,
                                  ),
                                ),
                              ),
                        Positioned(
                            bottom: 0,
                            right: -25,
                            child: RawMaterialButton(
                              onPressed: () {
                                _getFromGallery();
                              },
                              elevation: 2.0,
                              fillColor: Color(0xFFF5F6F9),
                              child: Icon(
                                Icons.camera_alt_outlined,
                                color: Colors.blue,
                              ),
                              padding: EdgeInsets.all(15.0),
                              shape: CircleBorder(),
                            )),
                      ],
                    ),
                  ),
                ),
                Form(
                    key: _formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'ID',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 16,
                            color: Colors.black54,
                          ),
                        ),
                        Container(
                          height: 40,
                          child: TextFormField(
                            controller: IDController,
                            decoration: InputDecoration(
                              filled: true,
                              errorStyle: const TextStyle(fontSize: 0.01),
                              contentPadding: EdgeInsets.only(
                                  left: 10.0,
                                  top: 0.0,
                                  bottom: 0.0,
                                  right: 0.0),
                              fillColor: Colors.white,
                              border: OutlineInputBorder(),
                              hintText: "ID",
                              hintStyle: TextStyle(fontSize: 14),
                            ),
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'invalid';
                              }
                              return null;
                            },
                          ),
                        ),
                        Text(
                          'Name',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 16,
                            color: Colors.black54,
                          ),
                        ),
                        SizedBox(
                          height: 40,
                          child: TextFormField(
                            controller: nameController,
                            decoration: InputDecoration(
                              filled: true,
                              errorStyle: const TextStyle(fontSize: 0.01),
                              contentPadding: EdgeInsets.only(
                                  left: 10.0,
                                  top: 0.0,
                                  bottom: 0.0,
                                  right: 0.0),
                              fillColor: Colors.white,
                              border: OutlineInputBorder(),
                              hintText: "Name",
                              hintStyle: TextStyle(fontSize: 14),
                            ),
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'invalid';
                              }
                              return null;
                            },
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          'Phone',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 16,
                            color: Colors.black54,
                          ),
                        ),
                        SizedBox(
                          height: 40,
                          child: TextFormField(
                            controller: phoneController,
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                              errorStyle: const TextStyle(fontSize: 0.01),
                              filled: true,
                              contentPadding: EdgeInsets.only(
                                  left: 10.0,
                                  top: 0.0,
                                  bottom: 0.0,
                                  right: 0.0),
                              fillColor: Colors.white,
                              border: OutlineInputBorder(),
                              hintText: "Phone",
                              hintStyle: TextStyle(fontSize: 14),
                            ),
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'invalid';
                              }
                              return null;
                            },
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          'Email',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 16,
                            color: Colors.black54,
                          ),
                        ),
                        SizedBox(
                          height: 40,
                          child: TextFormField(
                            controller: emailController,
                            decoration: InputDecoration(
                              errorStyle: const TextStyle(fontSize: 0.01),
                              filled: true,
                              contentPadding: EdgeInsets.only(
                                  left: 10.0,
                                  top: 0.0,
                                  bottom: 0.0,
                                  right: 0.0),
                              fillColor: Colors.white,
                              border: OutlineInputBorder(),
                              hintText: "Email",
                              hintStyle: TextStyle(fontSize: 14),
                            ),
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'invalid';
                              }
                              return null;
                            },
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          'Billing Address',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 16,
                            color: Colors.black54,
                          ),
                        ),
                        SizedBox(
                          // height: 40,
                          child: TextFormField(
                            controller: addressController,
                            maxLines: 4,
                            decoration: InputDecoration(
                              errorStyle: const TextStyle(fontSize: 0.01),
                              filled: true,
                              contentPadding: EdgeInsets.only(
                                  left: 10.0,
                                  top: 10.0,
                                  bottom: 0.0,
                                  right: 0.0),
                              fillColor: Colors.white,
                              border: OutlineInputBorder(),
                              hintText: "Address",
                              hintStyle: TextStyle(fontSize: 14),
                            ),
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'invalid';
                              }
                              return null;
                            },
                          ),
                        ),
                      ],
                    )),
                Align(
                    alignment: Alignment.centerRight,
                    child: ElevatedButton(
                        onPressed: () {
                          if (_formKey.currentState!.validate()) {}
                        },
                        child: Text("Update")))
              ],
            ),
          ),
        ),
      ),
    );
  }

  File? imageFile;

  _getFromGallery() async {
    XFile? pickedFile = await ImagePicker().pickImage(
      source: ImageSource.gallery,
      maxWidth: 1800,
      maxHeight: 1800,
    );
    if (pickedFile != null) {
      File imageFile = File(pickedFile.path);
    }
    setState(() {
      imageFile = File(pickedFile!.path);
    });
  }

  // File? image;
  // Future pickImage() async {
  //   try {
  //     final image = await ImagePicker().pickImage(source: ImageSource.gallery);
  //     if (image == null) return;
  //     final imageTemp = File(image.path);
  //     setState(() => this.image = imageTemp);
  //   } on PlatformException catch (e) {
  //     print('Failed to pick image: $e');
  //   }
  // }

  // XFile? _courseImage;
  // chooseImageFromGC() async {
  //   ImagePicker picker = ImagePicker();
  //   _courseImage = await picker.pickImage(source: ImageSource.gallery);
  //   setState(() {});
  // }
}
