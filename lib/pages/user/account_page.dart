import 'package:cart_and_hive/pages/user/account_details_page.dart';
import 'package:flutter/material.dart';

//======================this page is not needed======================

class AccountPage extends StatefulWidget {
  const AccountPage({super.key});

  @override
  State<AccountPage> createState() => _AccountPageState();
}

class _AccountPageState extends State<AccountPage> {
  @override
  Widget build(BuildContext context) {
    double textFontSize = 16;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromARGB(255, 87, 117, 133),
        toolbarHeight: 45,
        title: const Text('Account'),
      ),
      body: Container(
        padding: EdgeInsets.only(left: 10, right: 10, top: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // Divider(
            //   color: Colors.blue[50],
            //   endIndent: 0,
            //   height: 20,
            //   indent: 0,
            //   thickness: 2,
            // ),
            ElevatedButton(
              onPressed: () {},
              style: ElevatedButton.styleFrom(
                elevation: 0,
                backgroundColor: Colors.white,
                minimumSize: const Size.fromHeight(45),
              ),
              child: Align(
                alignment: Alignment.centerLeft,
                child: InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => AccountDetailsPage(),
                        )).then((value) {
                      setState(() {});
                    });
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "My Dashboard",
                        style: TextStyle(
                          fontSize: textFontSize,
                          color: Colors.black54,
                        ),
                      ),
                      Icon(
                        Icons.arrow_forward_ios,
                        color: Colors.black54,
                      )
                    ],
                  ),
                ),
              ),
            ),
            ElevatedButton(
              onPressed: () {},
              style: ElevatedButton.styleFrom(
                elevation: 0,
                backgroundColor: Colors.white,
                minimumSize: const Size.fromHeight(45),
              ),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Address Book",
                        style: TextStyle(
                          fontSize: textFontSize,
                          color: Colors.black54,
                        ),
                      ),
                      Icon(
                        Icons.arrow_forward_ios,
                        color: Colors.black54,
                      )
                    ],
                  )
                ],
              ),
            ),

            ElevatedButton(
              onPressed: () {},
              style: ElevatedButton.styleFrom(
                elevation: 0,
                backgroundColor: Colors.white,
                minimumSize: const Size.fromHeight(45),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Policies",
                    style: TextStyle(
                      fontSize: textFontSize,
                      color: Colors.black54,
                    ),
                  ),
                  Icon(
                    Icons.arrow_forward_ios,
                    color: Colors.black54,
                  )
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),

            ElevatedButton(
              onPressed: () {},
              style: ElevatedButton.styleFrom(
                elevation: 0,
                backgroundColor: Colors.white,
                minimumSize: const Size.fromHeight(45),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Logout",
                    style: TextStyle(
                      fontSize: textFontSize,
                      color: Colors.orange,
                    ),
                  ),
                  Icon(
                    Icons.logout,
                    color: Colors.orange,
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
