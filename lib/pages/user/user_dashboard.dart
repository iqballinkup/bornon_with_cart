import 'package:cart_and_hive/page_view_page.dart';
import 'package:cart_and_hive/widgets/drawer.dart';
import 'package:flutter/material.dart';

class UserDashboard extends StatefulWidget {
  const UserDashboard({super.key});

  @override
  State<UserDashboard> createState() => _UserDashboardState();
}

class _UserDashboardState extends State<UserDashboard> {
  TextEditingController couponController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var screenHeight = MediaQuery.of(context).size.height;
    var screenWidth = MediaQuery.of(context).size.width;

    double textFontSize = 16.0;

    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 45,
        backgroundColor: Color.fromARGB(255, 87, 117, 133),
        title: const Text('User Dashboard'),
      ),
      drawer: MyCustomDrawer(),
      body: Container(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Center(
                  child: CircleAvatar(
                      radius: 50,
                      backgroundColor: Colors.grey,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(50),
                        child: Image.asset(
                          "images/men_prod/product4.jpg",
                          fit: BoxFit.cover,
                        ),
                      )),
                ),
                SizedBox(
                  height: 20.0,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Table(
                      border:
                          TableBorder.all(color: Colors.black54, width: 1.5),
                      columnWidths: {
                        0: FlexColumnWidth(3),
                        1: FlexColumnWidth(5),
                      },
                      children: [
                        TableRow(children: [
                          Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Text(
                              "Id",
                              style: TextStyle(
                                  color: Colors.black54,
                                  fontSize: textFontSize,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Text(
                              "4965290897",
                              style: TextStyle(
                                color: Colors.black54,
                                fontSize: textFontSize,
                              ),
                            ),
                          ),
                        ]),
                        TableRow(children: [
                          Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Text(
                              "Name",
                              style: TextStyle(
                                  color: Colors.black54,
                                  fontSize: textFontSize,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Text(
                              "Iqbal Riaz",
                              style: TextStyle(
                                color: Colors.black54,
                                fontSize: textFontSize,
                              ),
                            ),
                          ),
                        ]),
                        TableRow(children: [
                          Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Text(
                              "Phone",
                              style: TextStyle(
                                  color: Colors.black54,
                                  fontSize: textFontSize,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Text(
                              "0123456789",
                              style: TextStyle(
                                color: Colors.black54,
                                fontSize: textFontSize,
                              ),
                            ),
                          ),
                        ]),
                        TableRow(children: [
                          Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Text(
                              "Address",
                              style: TextStyle(
                                  color: Colors.black54,
                                  fontSize: textFontSize,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Text(
                              "Cumilla",
                              style: TextStyle(
                                color: Colors.black54,
                                fontSize: textFontSize,
                              ),
                            ),
                          ),
                        ]),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Center(
                        child: ElevatedButton(
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => PageViewPage(),
                                  ));
                            },
                            child: Text("Logout")))
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
