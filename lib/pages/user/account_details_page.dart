import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

class AccountDetailsPage extends StatefulWidget {
  const AccountDetailsPage({super.key});

  @override
  State<AccountDetailsPage> createState() => _AccountDetailsPageState();
}

class _AccountDetailsPageState extends State<AccountDetailsPage> {
  TextEditingController _nameController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  TextEditingController _mobileController = TextEditingController();
  TextEditingController _emailController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromARGB(255, 87, 117, 133),
        toolbarHeight: 45,
        title: const Text('Account Details'),
      ),
      body: Container(
        padding: EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              color: Color.fromARGB(255, 146, 171, 185),
              height: 30,
              width: double.infinity,
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "General",
                  style: TextStyle(fontSize: 18, color: Colors.black54),
                ),
              ),
            ),
            TextFormField(
              controller: _nameController,
              decoration: InputDecoration(label: Text("Full Name")),
            ),
            TextFormField(
              controller: _nameController,
              decoration: InputDecoration(label: Text("Mobile")),
            ),
            TextFormField(
              controller: _nameController,
              decoration: InputDecoration(label: Text("Email")),
            ),
            Container(
              color: Color.fromARGB(255, 146, 171, 185),
              height: 30,
              width: double.infinity,
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Change Password",
                  style: TextStyle(fontSize: 18, color: Colors.black54),
                ),
              ),
            ),
            TextFormField(
              controller: _nameController,
              decoration: InputDecoration(label: Text("old Password")),
            ),
          ],
        ),
      ),
    );
  }
}
