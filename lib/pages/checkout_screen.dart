import 'package:cart_and_hive/pages/user/update_profile_page.dart';
import 'package:cart_and_hive/widgets/custom_base_widget/custom_text_field.dart';
import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';

class CheckoutScreen extends StatefulWidget {
  late String checkoutFrom;
  String? productName;
  int? productUnitPrice, productQuantity, productSubtotalPrice, productTotalPrice;
  CheckoutScreen({
    super.key,
    required this.checkoutFrom,
    this.productName,
    this.productUnitPrice,
    this.productQuantity,
    this.productSubtotalPrice,
    this.productTotalPrice,
  });

  @override
  State<CheckoutScreen> createState() => _CheckoutScreenState();
}

class _CheckoutScreenState extends State<CheckoutScreen> {
  double h2TextSize = 14.0;
  List<String> _locations = ['Uttara', 'Gulshan', 'Banani', 'Dhanmondi'];
  String? _selectedLocation;
  bool showWalletPay = false;
  String deliveryOptions = "Cash on delivery";

  final _formKey = GlobalKey<FormState>();

  TextEditingController _shipperNameController = TextEditingController();
  TextEditingController _shipperPhoneController = TextEditingController();
  TextEditingController _shipperAddressController = TextEditingController();
  TextEditingController _orderNoteController = TextEditingController();

  final _listViewController = ScrollController();
  final _scrollViewController = ScrollController();
  late final Box box;
  double shippingFee = 60;
  bool isWalletAdjusted = false;
  int _selectedRadioButton = 0;

  @override
  void initState() {
    _listViewController.addListener(_onListScroll);
    box = Hive.box('productBox');
    print("chekout from==${widget.checkoutFrom}");
    // print("productName==${widget.productName}");
    // print("productQuantity==${widget.productQuantity}");
    // print("productUnitPrice==${widget.productUnitPrice}");
    // print("productSubtotalPrice==${widget.productSubtotalPrice}");
    // print("productTotalPrice==${widget.productTotalPrice}");
    // print("=============================");
    super.initState();
  }

  @override
  void dispose() {
    _listViewController.removeListener(_onListScroll);
    super.dispose();
  }

  void _onListScroll() {
    if (_listViewController.position.pixels == _listViewController.position.maxScrollExtent) {
      _scrollViewController.animateTo(
        _scrollViewController.position.maxScrollExtent,
        duration: const Duration(milliseconds: 50),
        curve: Curves.easeIn,
      );
    }

    if (_listViewController.position.pixels == _listViewController.position.minScrollExtent) {
      _scrollViewController.animateTo(
        _scrollViewController.position.minScrollExtent,
        duration: const Duration(milliseconds: 50),
        curve: Curves.easeIn,
      );
    }
  }

  //==============================================

  @override
  Widget build(BuildContext context) {
    var screenHeight = MediaQuery.of(context).size.height;
    var screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(Icons.arrow_back)),
        toolbarHeight: 45,
        backgroundColor: Color.fromARGB(255, 87, 117, 133),
        title: Text('Order Summery'),
      ),
      body: SingleChildScrollView(
          controller: _scrollViewController,
          child: widget.checkoutFrom == "cart"
              ? Container(
                  padding: EdgeInsets.all(10),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ListView.builder(
                        controller: _listViewController,
                        physics: ClampingScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: box.length,
                        itemBuilder: (context, index) {
                          var currentBox = box;
                          var productData = currentBox.getAt(index)!;
                          print(
                            "product length: ${box.length}",
                          );
                          return Column(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Container(
                                    width: screenWidth / 3 + 20,
                                    child: Text(
                                      "${productData.productName}",
                                      style: TextStyle(fontSize: h2TextSize, fontWeight: FontWeight.bold, color: Colors.black38),
                                    ),
                                  ),
                                  Container(
                                    width: screenWidth / 12,
                                    child: Text(
                                      "${productData.productQuantity}",
                                      style: TextStyle(fontSize: h2TextSize, fontWeight: FontWeight.bold, color: Colors.black38),
                                    ),
                                  ),
                                  Container(
                                    width: screenWidth / 5,
                                    child: Text(
                                      "৳${productData.productPrice}",
                                      textAlign: TextAlign.start,
                                      style: TextStyle(fontSize: h2TextSize, fontWeight: FontWeight.bold, color: Colors.black38),
                                    ),
                                  ),
                                  Container(
                                      width: screenWidth / 4,
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.end,
                                        children: [
                                          Text(
                                            "৳",
                                            style: TextStyle(fontSize: h2TextSize, fontWeight: FontWeight.bold, color: Colors.black38),
                                          ),
                                          Text(
                                            "${productData.productPrice * productData.productQuantity}",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(fontSize: h2TextSize, fontWeight: FontWeight.bold, color: Colors.black38),
                                          ),
                                        ],
                                      ))
                                ],
                              ),
                              Divider()
                            ],
                          );
                        },
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        margin: EdgeInsets.only(bottom: 15),
                        decoration: BoxDecoration(
                          color: const Color.fromARGB(255, 231, 231, 231),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: ListView.builder(
                          itemCount: 1,
                          shrinkWrap: true,
                          physics: ClampingScrollPhysics(),
                          itemBuilder: (BuildContext context, int index) {
                            num totalPrice = 0;
                            for (int i = 0; i < box.length; i++) {
                              var productData = box.getAt(i)!;
                              totalPrice += productData.productPrice * productData.productQuantity;
                            }
                            return Container(
                              padding: EdgeInsets.all(20),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        "Subtotal: ",
                                        style: TextStyle(
                                          fontSize: h2TextSize,
                                        ),
                                      ),
                                      Text(
                                        "৳$totalPrice",
                                        style: TextStyle(fontSize: h2TextSize, fontWeight: FontWeight.bold, color: Colors.black38),
                                      )
                                    ],
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        "Vat 15% (+): ",
                                        style: TextStyle(
                                          fontSize: h2TextSize,
                                        ),
                                      ),
                                      Text(
                                        "180.00",
                                        style: TextStyle(fontSize: h2TextSize, fontWeight: FontWeight.bold, color: Colors.black38),
                                      )
                                    ],
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        "Delivery Charge (+):",
                                        style: TextStyle(
                                          fontSize: h2TextSize,
                                        ),
                                      ),
                                      Text(
                                        "৳ $shippingFee",
                                        style: TextStyle(fontSize: h2TextSize, fontWeight: FontWeight.bold, color: Colors.black38),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        "Area:",
                                        style: TextStyle(
                                          fontSize: h2TextSize,
                                        ),
                                      ),
                                      DropdownButton(
                                        hint: Text(
                                          'Please choose a location',
                                          style: TextStyle(
                                            fontSize: h2TextSize,
                                          ),
                                        ), // Not necessary for Option 1
                                        value: _selectedLocation,
                                        onChanged: (newValue) {
                                          setState(() {
                                            _selectedLocation = newValue!;
                                          });
                                        },
                                        items: _locations.map((location) {
                                          return DropdownMenuItem(
                                            child: Text(
                                              location,
                                              style: TextStyle(
                                                fontSize: h2TextSize,
                                              ),
                                            ),
                                            value: location,
                                          );
                                        }).toList(),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        "Total :",
                                        style: TextStyle(
                                          fontSize: h2TextSize,
                                        ),
                                      ),
                                      Text(
                                        "৳${(totalPrice + shippingFee).toString()}",
                                        style: TextStyle(fontSize: h2TextSize, color: Colors.black),
                                      ),
                                    ],
                                  ),
                                  showWalletPay == true
                                      ? Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              "Wallet Pay :",
                                              style: TextStyle(
                                                fontSize: h2TextSize,
                                              ),
                                            ),
                                            Text(
                                              "33.0",
                                              style: TextStyle(fontSize: h2TextSize, color: Colors.black),
                                            ),
                                          ],
                                        )
                                      : Container(),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        "Payable :",
                                        style: TextStyle(
                                          fontSize: h2TextSize,
                                        ),
                                      ),
                                      Text(
                                        "৳${(totalPrice + shippingFee).toString()}",
                                        style: TextStyle(fontSize: h2TextSize, fontWeight: FontWeight.bold, color: Colors.black),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    children: [
                                      Checkbox(
                                        checkColor: Colors.white,
                                        activeColor: Colors.black,
                                        value: isWalletAdjusted,
                                        onChanged: (bool? value) {
                                          setState(() {
                                            isWalletAdjusted = value!;
                                            if (isWalletAdjusted) {
                                              showWalletPay = true;
                                            } else {
                                              showWalletPay = false;
                                            }
                                          });
                                        },
                                      ),
                                      SizedBox(
                                        width: 5.0,
                                      ),
                                      Text(
                                        'Wallet Adjustment (33%)',
                                        style: TextStyle(
                                          fontSize: screenWidth / 24,
                                          color: Colors.lightGreen,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    children: [
                                      Expanded(
                                        flex: 1,
                                        child: Row(
                                          children: [
                                            Radio(
                                              value: 0,
                                              groupValue: _selectedRadioButton,
                                              onChanged: (value) {
                                                setState(() {
                                                  _selectedRadioButton = value!;
                                                });
                                              },
                                            ),
                                            Text(
                                              'Cash on delivery',
                                              style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                color: Colors.black54,
                                                fontSize: screenWidth / 30,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Expanded(
                                        flex: 1,
                                        child: Row(
                                          children: [
                                            Radio(
                                              value: 1,
                                              groupValue: _selectedRadioButton,
                                              onChanged: (value) {
                                                setState(() {
                                                  _selectedRadioButton = value!;
                                                });
                                              },
                                            ),
                                            Text(
                                              'Online Payment',
                                              style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                color: Colors.black54,
                                                fontSize: screenWidth / 30,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            );
                          },
                        ),
                      ),
                      Text('Shipping Information', style: TextStyle(fontSize: h2TextSize, fontWeight: FontWeight.bold, color: Colors.black54)),
                      Container(
                        margin: EdgeInsets.only(top: 15),
                        child: Form(
                          key: _formKey,
                          child: Column(
                            children: [
                              Container(
                                padding: const EdgeInsets.all(8.0),
                                decoration: BoxDecoration(color: Color.fromARGB(255, 231, 231, 231), borderRadius: BorderRadius.circular(10)),
                                child: Column(
                                  children: [
                                    Row(
                                      children: [
                                        Expanded(flex: 1, child: Text("Name *", style: TextStyle(fontSize: h2TextSize))),
                                        SizedBox(
                                          width: 5,
                                        ),
                                        Expanded(
                                            flex: 3,
                                            child: MyCustomTextFormField().getCustomEditTextArea(
                                                hintValue: 'Name',
                                                validation: true,
                                                controller: _shipperNameController,
                                                keyboardType: TextInputType.text,
                                                validationErrorMsg: 'error_msg')),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Expanded(flex: 1, child: Text("Phone number *", style: TextStyle(fontSize: h2TextSize))),
                                        SizedBox(
                                          width: 5,
                                        ),
                                        Expanded(
                                          flex: 3,
                                          child: TextFormField(
                                            controller: _shipperPhoneController,
                                            validator: (value) {
                                              if (value == null || value.isEmpty) {
                                                return "invalid";
                                              }
                                              return null;
                                            },
                                            keyboardType: TextInputType.number,
                                            decoration: InputDecoration(
                                              errorStyle: TextStyle(fontSize: 0.01),
                                              hintText: "Phone number",
                                              hintStyle: TextStyle(fontSize: h2TextSize),
                                              contentPadding: EdgeInsets.only(left: 10.0, top: 0.0, bottom: 0.0, right: 0.0),
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Expanded(flex: 1, child: Text("Shipping Address *", style: TextStyle(fontSize: h2TextSize))),
                                        SizedBox(
                                          width: 5,
                                        ),
                                        Expanded(
                                          flex: 3,
                                          child: TextFormField(
                                            controller: _shipperAddressController,
                                            validator: (value) {
                                              if (value == null || value.isEmpty) {
                                                return "invalid";
                                              }
                                              return null;
                                            },
                                            decoration: InputDecoration(
                                              hintText: "Shipping Address",
                                              hintStyle: TextStyle(fontSize: h2TextSize),
                                              errorStyle: TextStyle(fontSize: 0.01),
                                              contentPadding: EdgeInsets.only(left: 10.0, top: 0.0, bottom: 0.0, right: 0.0),
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Row(
                                      children: [
                                        Expanded(flex: 1, child: Text("Order notes (optional)", style: TextStyle(fontSize: h2TextSize))),
                                        SizedBox(
                                          width: 5,
                                        ),
                                        Expanded(
                                          flex: 3,
                                          child: TextFormField(
                                            controller: _shipperAddressController,
                                            maxLines: 2,
                                            decoration: InputDecoration(
                                              hintText: "Notes about your order, e.g. special notes for delivery",
                                              hintStyle: TextStyle(fontSize: h2TextSize),
                                              errorStyle: TextStyle(fontSize: 0.01),
                                              contentPadding: EdgeInsets.only(left: 10.0, top: 0.0, bottom: 0.0, right: 0.0),
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Align(
                                        alignment: Alignment.centerRight,
                                        child: ElevatedButton(
                                            onPressed: () {
                                              if (_formKey.currentState!.validate()) {}
                                            },
                                            child: Text("Update")))
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                    ],
                  ),
                )
              : Container(
                  padding: EdgeInsets.all(10),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            width: screenWidth / 3 + 20,
                            child: Text(
                              "${widget.productName}",
                              style: TextStyle(fontSize: h2TextSize, fontWeight: FontWeight.bold, color: Colors.black38),
                            ),
                          ),
                          Container(
                            width: screenWidth / 12,
                            child: Text(
                              "${widget.productQuantity}",
                              style: TextStyle(fontSize: h2TextSize, fontWeight: FontWeight.bold, color: Colors.black38),
                            ),
                          ),
                          Container(
                            width: screenWidth / 5,
                            child: Text(
                              "৳${widget.productUnitPrice}",
                              textAlign: TextAlign.start,
                              style: TextStyle(fontSize: h2TextSize, fontWeight: FontWeight.bold, color: Colors.black38),
                            ),
                          ),
                          Container(
                              width: screenWidth / 4,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Text(
                                    "৳",
                                    style: TextStyle(fontSize: h2TextSize, fontWeight: FontWeight.bold, color: Colors.black38),
                                  ),
                                  Text(
                                    "${widget.productSubtotalPrice}",
                                    textAlign: TextAlign.start,
                                    style: TextStyle(fontSize: h2TextSize, fontWeight: FontWeight.bold, color: Colors.black38),
                                  ),
                                ],
                              ))
                        ],
                      ),
                      Divider(),

                      //Bill Section
                      Container(
                        height: 90,
                        decoration: BoxDecoration(color: const Color.fromARGB(255, 231, 231, 231), borderRadius: BorderRadius.circular(10)),
                        child: ListView.builder(
                          itemCount: 1,
                          itemBuilder: (BuildContext context, int index) {
                            return Center(
                              child: Container(
                                padding: EdgeInsets.only(top: 10, left: 12, right: 12, bottom: 20),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          "Subtotal: ",
                                          style: TextStyle(fontSize: h2TextSize, fontWeight: FontWeight.bold, color: Colors.black38),
                                        ),
                                        Text(
                                          "৳${widget.productSubtotalPrice}",
                                          style: TextStyle(fontSize: h2TextSize, fontWeight: FontWeight.bold, color: Colors.black38),
                                        )
                                      ],
                                    ),
                                    Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                                      Text(
                                        "Shipping cost:",
                                        style: TextStyle(fontSize: h2TextSize, fontWeight: FontWeight.bold, color: Colors.black38),
                                      ),
                                      Text(
                                        "৳ $shippingFee",
                                        style: TextStyle(fontSize: h2TextSize, fontWeight: FontWeight.bold, color: Colors.black38),
                                      ),
                                    ]),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          "Total Price:",
                                          style: TextStyle(
                                            fontSize: h2TextSize,
                                          ),
                                        ),
                                        Text(
                                          "৳${(widget.productTotalPrice! + shippingFee)}",
                                          style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, color: Colors.black),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            );
                          },
                        ),
                      ),

                      Text('Shipping Information', style: TextStyle(fontSize: h2TextSize, fontWeight: FontWeight.bold, color: Colors.black54)),

                      Container(
                        margin: EdgeInsets.only(top: 15),
                        child: Form(
                          key: _formKey,
                          child: Column(
                            children: [
                              Container(
                                padding: const EdgeInsets.all(8.0),
                                decoration: BoxDecoration(color: Color.fromARGB(255, 231, 231, 231), borderRadius: BorderRadius.circular(10)),
                                child: Column(
                                  children: [
                                    Row(
                                      children: [
                                        Expanded(flex: 1, child: Text("Full Name")),
                                        SizedBox(
                                          width: 5,
                                        ),
                                        Expanded(
                                          flex: 3,
                                          child: TextFormField(
                                            controller: _shipperNameController,
                                            validator: (value) {
                                              if (value == null || value.isEmpty) {
                                                return 'invalid';
                                              }
                                              return null;
                                            },
                                            decoration: InputDecoration(
                                              hintText: "name",
                                              errorStyle: const TextStyle(fontSize: 0.01),
                                              contentPadding: EdgeInsets.only(left: 10.0, top: 0.0, bottom: 0.0, right: 0.0),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Expanded(flex: 1, child: Text("Mobile")),
                                        SizedBox(
                                          width: 5,
                                        ),
                                        Expanded(
                                          flex: 3,
                                          child: TextFormField(
                                            controller: _shipperPhoneController,
                                            validator: (value) {
                                              if (value == null || value.isEmpty) {
                                                return "invalid";
                                              }
                                              return null;
                                            },
                                            keyboardType: TextInputType.number,
                                            decoration: InputDecoration(
                                              errorStyle: TextStyle(fontSize: 0.01),
                                              hintText: "mobile",
                                              contentPadding: EdgeInsets.only(left: 10.0, top: 0.0, bottom: 0.0, right: 0.0),
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Expanded(flex: 1, child: Text("Address")),
                                        SizedBox(
                                          width: 5,
                                        ),
                                        Expanded(
                                          flex: 3,
                                          child: TextFormField(
                                            controller: _shipperAddressController,
                                            validator: (value) {
                                              if (value == null || value.isEmpty) {
                                                return "invalid";
                                              }
                                              return null;
                                            },
                                            decoration: InputDecoration(
                                              hintText: "address",
                                              errorStyle: TextStyle(fontSize: 0.01),
                                              contentPadding: EdgeInsets.only(left: 10.0, top: 0.0, bottom: 0.0, right: 0.0),
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Align(
                                        alignment: Alignment.centerRight,
                                        child: ElevatedButton(
                                            onPressed: () {
                                              if (_formKey.currentState!.validate()) {}
                                            },
                                            child: Text("Update")))
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                )),
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.only(left: 20, right: 20, bottom: 10.0),
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(backgroundColor: Color.fromARGB(255, 60, 84, 97), minimumSize: const Size.fromHeight(45)),
          onPressed: () {
            // Navigator.push(
            //     context,
            //     MaterialPageRoute(
            //       builder: (context) => CheckoutScreen(),
            //     ));
          },
          child: const Text("Proceed to Pay"),
        ),
      ),
    );
  }
}
