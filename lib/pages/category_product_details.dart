import 'package:cart_and_hive/model/hive/product.dart';
import 'package:cart_and_hive/pages/checkout_screen.dart';
import 'package:cart_and_hive/widgets/shoping_cart_badge.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';

class CategoryProductDetails extends StatefulWidget {
  // final Map<String, dynamic> kidsProducts;
  final String catProName, catProImage;
  final int catProPrice;
  int catProQuantity;
  int catProIndex;
  CategoryProductDetails({
    super.key,
    required this.catProPrice,
    required this.catProName,
    required this.catProImage,
    required this.catProQuantity,
    required this.catProIndex,
  });

  @override
  State<CategoryProductDetails> createState() => _CategoryProductDetailsState();
}

class _CategoryProductDetailsState extends State<CategoryProductDetails> {
  int categoryId = 1;
  int selectedSizeButton = 1;
  double headingTextSize = 16.0;
  double descriptionTextSize = 14.0;
  String productDetailsText = "This is the details of the products.";

  late final Box box;

  @override
  void initState() {
    super.initState();
    print(widget.catProPrice);
    print(widget.catProImage);
    print(widget.catProIndex);
    print(widget.catProName);
    print(widget.catProQuantity);
    // Get reference to an already opened box
    box = Hive.box('productBox');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 226, 226, 226),
      appBar: AppBar(
        backgroundColor: Color.fromARGB(255, 87, 117, 133),
        toolbarHeight: 45,
        title: const Text('Product Details'),
        actions: [
          Padding(
            padding: const EdgeInsets.only(top: 5.0, bottom: 5.0, right: 10.0),
            child: ShoppingCartBadge(),
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
                flex: 4,
                child: Row(
                  children: [
                    Expanded(
                        flex: 3,
                        child: Container(
                          child: Image.asset(
                            widget.catProImage,
                            fit: BoxFit.fill,
                          ),
                        )),
                    Expanded(
                        flex: 1,
                        child: ListView.builder(
                          itemCount: 5,
                          scrollDirection: Axis.vertical,
                          itemBuilder: (context, index) {
                            return Container(
                                padding: EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(10)),
                                margin: const EdgeInsets.only(
                                    left: 10, right: 5, bottom: 10),
                                child: Image.asset(
                                  widget.catProImage,
                                  fit: BoxFit.fill,
                                ));
                          },
                        ))
                  ],
                )),
            Expanded(
              flex: 4,
              child: Container(
                margin: EdgeInsets.only(top: 10),
                width: double.infinity,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Text(
                              widget.catProName,
                              style: TextStyle(
                                  fontSize: headingTextSize,
                                  fontWeight: FontWeight.bold),
                            ),
                            Container(
                                margin: EdgeInsets.only(left: 10, right: 10),
                                height: 22,
                                width: 22,
                                decoration: BoxDecoration(
                                    color: Colors.transparent,
                                    border: Border.all(color: Colors.black),
                                    borderRadius: BorderRadius.circular(20)),
                                child: IconButton(
                                    padding: EdgeInsets.zero,
                                    onPressed: (() {
                                      setState(() {
                                        widget.catProQuantity > 1
                                            ? widget.catProQuantity--
                                            : widget.catProQuantity;
                                      });
                                    }),
                                    icon: Icon(
                                      Icons.remove,
                                      size: 15,
                                    ))),
                            Text(
                              widget.catProQuantity.toString(),
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.bold),
                            ),
                            Container(
                                margin: EdgeInsets.only(
                                  left: 10,
                                ),
                                height: 22,
                                width: 22,
                                decoration: BoxDecoration(
                                    color: Colors.transparent,
                                    border: Border.all(color: Colors.black),
                                    borderRadius: BorderRadius.circular(20)),
                                child: IconButton(
                                    padding: EdgeInsets.zero,
                                    onPressed: (() {
                                      setState(() {
                                        widget.catProQuantity > 0
                                            ? widget.catProQuantity++
                                            : widget.catProQuantity;
                                      });
                                    }),
                                    icon: Icon(
                                      Icons.add,
                                      size: 15,
                                    )))
                          ],
                        ),
                        Row(
                          children: [
                            Icon(
                              Icons.favorite,
                              color: Colors.red,
                            ),
                            Text(
                              "Add to wishlist",
                              style: TextStyle(
                                fontSize: descriptionTextSize,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Short Details:",
                      style: TextStyle(
                        fontSize: headingTextSize,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 250.0),
                      child: Divider(
                        color: Colors.black,
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      productDetailsText,
                      style: TextStyle(
                        fontSize: descriptionTextSize,
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Row(
                      children: [
                        Text(
                          "Price: ",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: descriptionTextSize,
                          ),
                        ),
                        Text(
                          "৳${widget.catProPrice.toString()}",
                          style: TextStyle(
                            fontSize: descriptionTextSize,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Row(
                      children: [
                        Text(
                          "Category ID: ",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: descriptionTextSize,
                          ),
                        ),
                        Text(
                          categoryId.toString(),
                          style: TextStyle(
                            fontSize: descriptionTextSize,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Row(
                      children: [
                        Text(
                          "Color",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: descriptionTextSize,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 10, right: 10),
                          height: 18,
                          width: 30,
                          decoration: BoxDecoration(
                              color: Colors.black,
                              border: Border.all(color: Colors.black),
                              borderRadius: BorderRadius.circular(10)),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Row(
                      children: [
                        Text(
                          "Size",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: descriptionTextSize,
                          ),
                        ),
                        InkWell(
                          onTap: (() {
                            setState(() {
                              selectedSizeButton = 1;
                            });
                          }),
                          child: Container(
                              margin: EdgeInsets.only(left: 5, right: 5),
                              height: 28,
                              width: 28,
                              decoration: BoxDecoration(
                                  color: selectedSizeButton == 1
                                      ? Colors.red[600]
                                      : Colors.pink[300],
                                  borderRadius: BorderRadius.circular(20)),
                              child: Center(
                                child: Text(
                                  "S",
                                  style: TextStyle(color: Colors.white),
                                ),
                              )),
                        ),
                        InkWell(
                          onTap: (() {
                            setState(() {
                              selectedSizeButton = 2;
                            });
                          }),
                          child: Container(
                              margin: EdgeInsets.only(left: 5, right: 5),
                              height: 28,
                              width: 28,
                              decoration: BoxDecoration(
                                  color: selectedSizeButton == 2
                                      ? Colors.red[600]
                                      : Colors.pink[300],
                                  borderRadius: BorderRadius.circular(20)),
                              child: Center(
                                child: Text(
                                  "M",
                                  style: TextStyle(color: Colors.white),
                                ),
                              )),
                        ),
                        InkWell(
                          onTap: (() {
                            setState(() {
                              selectedSizeButton = 3;
                            });
                          }),
                          child: Container(
                              margin: EdgeInsets.only(left: 5, right: 5),
                              height: 28,
                              width: 28,
                              decoration: BoxDecoration(
                                  color: selectedSizeButton == 3
                                      ? Colors.red[600]
                                      : Colors.pink[300],
                                  borderRadius: BorderRadius.circular(20)),
                              child: Center(
                                child: Text(
                                  "XL",
                                  style: TextStyle(color: Colors.white),
                                ),
                              )),
                        ),
                        InkWell(
                          onTap: (() {
                            setState(() {
                              selectedSizeButton = 4;
                            });
                          }),
                          child: Container(
                              margin: EdgeInsets.only(left: 5, right: 5),
                              height: 28,
                              width: 28,
                              decoration: BoxDecoration(
                                  color: selectedSizeButton == 4
                                      ? Colors.red[600]
                                      : Colors.pink[300],
                                  borderRadius: BorderRadius.circular(20)),
                              child: Center(
                                child: Text(
                                  "XXL",
                                  style: TextStyle(color: Colors.white),
                                ),
                              )),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
            Expanded(
                flex: 1,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ElevatedButton(
                        onPressed: () {
                          num totalPrice = 0;
                          late int productData;
                          for (int i = 0; i < box.length; i++) {
                            var productData = box.getAt(i)!;
                            totalPrice += productData.productPrice *
                                productData.productQuantity;
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => CheckoutScreen(
                                    checkoutFrom: "categoryProductDetails",
                                      // productName: productData.productName,
                                      // productQuantity:
                                      //     productData.productQuantity,
                                      // productSubtotalPrice:
                                      //     productData.productPrice *
                                      //         productData.productQuantity,
                                      // productUnitPrice: productData.productPrice,
                                      // productTotalPrice: totalPrice.toInt(),
                                      ),
                                ));
                          }
                          ;
                        },
                        child: const Text("Check Out")),
                    SizedBox(
                      width: 30,
                    ),
                    ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.green),
                        onPressed: () {
                          bool found = false;
                          setState(() {
                            box.length;
                          });
                          for (int i = 0; i < box.length; i++) {
                            ProductDetails existingProduct = box.getAt(i);
                            if (existingProduct.productName ==
                                    widget.catProName &&
                                existingProduct.productPrice ==
                                    widget.catProPrice) {
                              existingProduct.productQuantity =
                                  widget.catProQuantity;
                              box.putAt(i, existingProduct);
                              found = true;
                              break;
                            }
                          }
                          if (!found) {
                            ProductDetails productDetails = ProductDetails(
                                productName: widget.catProName,
                                productPrice: widget.catProPrice,
                                productQuantity: widget.catProQuantity,
                                productImage: widget.catProImage);
                            box.add(productDetails);
                          }
                        },
                        child: Text("Add to Cart"))
                  ],
                )),
          ],
        ),
      ),
    );
  }
}
