import 'package:cart_and_hive/model/hive/product.dart';
import 'package:cart_and_hive/pages/cart_screen.dart';
import 'package:cart_and_hive/pages/checkout_screen.dart';
import 'package:cart_and_hive/widgets/shoping_cart_badge.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';

class ProductDetailsPage extends StatefulWidget {
  // final Map<String, dynamic> kidsProducts;
  final String? pName, pImage, pShortDetails;
  final int? pPrice;
  final String? pDescription;
  int pQuantity, pCategoryId;
  ProductDetailsPage({
    super.key,
    this.pPrice,
    required this.pName,
    required this.pImage,
    required this.pQuantity,
    required this.pShortDetails,
    required this.pDescription,
    required this.pCategoryId,
  });

  @override
  State<ProductDetailsPage> createState() => _ProductDetailsPageState();
}

class _ProductDetailsPageState extends State<ProductDetailsPage> {
  int categoryId = 1;
  int selectedSizeButton = 1;
  double headingTextSize = 16.0;
  double descriptionTextSize = 14.0;
  String productDetailsText = "This is the details of the products.";

  late final Box box;

  @override
  void initState() {
    super.initState();
    print("pName===${widget.pName}");
    print("pImage===${widget.pImage}");
    print("pPrice===${widget.pPrice}");
    print("pQuantity===${widget.pQuantity}");
    print("pCategoryId===${widget.pCategoryId}");
    print("pShortDetails===${widget.pShortDetails}");
    print("pDescription===${widget.pDescription}");
    // Get reference to an already opened box
    box = Hive.box('productBox');
  }

  @override
  Widget build(BuildContext context) {
    // var screenHeight = MediaQuery.of(context).size.height;
    // var screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
        backgroundColor: Color.fromARGB(255, 226, 226, 226),
        appBar: AppBar(
          backgroundColor: Color.fromARGB(255, 87, 117, 133),
          toolbarHeight: 45,
          title: const Text('Product Details'),
          actions: [
            Padding(
              padding: const EdgeInsets.only(top: 5.0, bottom: 5.0, right: 10.0),
              child: ShoppingCartBadge(),
            ),
          ],
        ),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                    height: 320,
                    width: double.infinity,
                    child: Row(
                      children: [
                        Container(
                          height: 320,
                          width: 250,
                          child: Image.network(
                            widget.pImage!,
                            fit: BoxFit.fill,
                          ),
                        ),
                        Container(
                            height: 320,
                            width: 90,
                            child: ListView.builder(
                              shrinkWrap: true,
                              itemCount: 5,
                              scrollDirection: Axis.vertical,
                              itemBuilder: (context, index) {
                                return Container(
                                    height: 80,
                                    padding: EdgeInsets.all(5),
                                    decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(10)),
                                    margin: const EdgeInsets.only(left: 10, right: 5, bottom: 10),
                                    child: Image.network(
                                      widget.pImage!,
                                      fit: BoxFit.fill,
                                    ));
                              },
                            ))
                      ],
                    )),
                Container(
                  child: Container(
                    margin: EdgeInsets.only(top: 10),
                    width: double.infinity,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                Text(
                                  widget.pName!,
                                  style: TextStyle(fontSize: headingTextSize, fontWeight: FontWeight.bold),
                                ),
                                Container(
                                    margin: EdgeInsets.only(left: 10, right: 10),
                                    height: 22,
                                    width: 22,
                                    decoration: BoxDecoration(
                                        color: Colors.transparent, border: Border.all(color: Colors.black), borderRadius: BorderRadius.circular(20)),
                                    child: IconButton(
                                        padding: EdgeInsets.zero,
                                        onPressed: (() {
                                          setState(() {
                                            widget.pQuantity > 1 ? widget.pQuantity-- : widget.pQuantity;
                                          });
                                        }),
                                        icon: Icon(
                                          Icons.remove,
                                          size: 15,
                                        ))),
                                Text(
                                  widget.pQuantity.toString(),
                                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                                ),
                                Container(
                                    margin: EdgeInsets.only(
                                      left: 10,
                                    ),
                                    height: 22,
                                    width: 22,
                                    decoration: BoxDecoration(
                                        color: Colors.transparent, border: Border.all(color: Colors.black), borderRadius: BorderRadius.circular(20)),
                                    child: IconButton(
                                        padding: EdgeInsets.zero,
                                        onPressed: (() {
                                          setState(() {
                                            widget.pQuantity > 0 ? widget.pQuantity++ : widget.pQuantity;
                                          });
                                        }),
                                        icon: Icon(
                                          Icons.add,
                                          size: 15,
                                        )))
                              ],
                            ),
                            Row(
                              children: [
                                Icon(
                                  Icons.favorite,
                                  color: Colors.red,
                                ),
                                Text(
                                  "Add to wishlist",
                                  style: TextStyle(
                                    fontSize: descriptionTextSize,
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          "Short Details:",
                          style: TextStyle(fontSize: headingTextSize, fontWeight: FontWeight.bold),
                        ),
                        Text(
                          widget.pShortDetails!,
                          style: TextStyle(
                            fontSize: descriptionTextSize,
                          ),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Row(
                          children: [
                            Text(
                              "Price: ",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: descriptionTextSize,
                              ),
                            ),
                            Text(
                              "৳${widget.pPrice.toString()}",
                              style: TextStyle(
                                fontSize: descriptionTextSize,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Row(
                          children: [
                            Text(
                              "Category ID: ",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: descriptionTextSize,
                              ),
                            ),
                            Text(
                              widget.pCategoryId.toString(),
                              style: TextStyle(
                                fontSize: descriptionTextSize,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Row(
                          children: [
                            Text(
                              "Color",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: descriptionTextSize,
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 10, right: 10),
                              height: 18,
                              width: 30,
                              decoration: BoxDecoration(
                                  color: Colors.black, border: Border.all(color: Colors.black), borderRadius: BorderRadius.circular(10)),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Row(
                          children: [
                            Text(
                              "Size",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: descriptionTextSize,
                              ),
                            ),
                            InkWell(
                              onTap: (() {
                                setState(() {
                                  selectedSizeButton = 1;
                                });
                              }),
                              child: Container(
                                  margin: EdgeInsets.only(left: 5, right: 5),
                                  height: 28,
                                  width: 28,
                                  decoration: BoxDecoration(
                                      color: selectedSizeButton == 1 ? Colors.red[600] : Colors.pink[300], borderRadius: BorderRadius.circular(20)),
                                  child: Center(
                                    child: Text(
                                      "S",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  )),
                            ),
                            InkWell(
                              onTap: (() {
                                setState(() {
                                  selectedSizeButton = 2;
                                });
                              }),
                              child: Container(
                                  margin: EdgeInsets.only(left: 5, right: 5),
                                  height: 28,
                                  width: 28,
                                  decoration: BoxDecoration(
                                      color: selectedSizeButton == 2 ? Colors.red[600] : Colors.pink[300], borderRadius: BorderRadius.circular(20)),
                                  child: Center(
                                    child: Text(
                                      "M",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  )),
                            ),
                            InkWell(
                              onTap: (() {
                                setState(() {
                                  selectedSizeButton = 3;
                                });
                              }),
                              child: Container(
                                  margin: EdgeInsets.only(left: 5, right: 5),
                                  height: 28,
                                  width: 28,
                                  decoration: BoxDecoration(
                                      color: selectedSizeButton == 3 ? Colors.red[600] : Colors.pink[300], borderRadius: BorderRadius.circular(20)),
                                  child: Center(
                                    child: Text(
                                      "XL",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  )),
                            ),
                            InkWell(
                              onTap: (() {
                                setState(() {
                                  selectedSizeButton = 4;
                                });
                              }),
                              child: Container(
                                  margin: EdgeInsets.only(left: 5, right: 5),
                                  height: 28,
                                  width: 28,
                                  decoration: BoxDecoration(
                                      color: selectedSizeButton == 4 ? Colors.red[600] : Colors.pink[300], borderRadius: BorderRadius.circular(20)),
                                  child: Center(
                                    child: Text(
                                      "XXL",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  )),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        Text(
                          "Description:",
                          style: TextStyle(fontSize: headingTextSize, fontWeight: FontWeight.bold),
                        ),
                        Text(
                          widget.pDescription!,
                          style: TextStyle(
                            fontSize: descriptionTextSize,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        bottomNavigationBar: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => CheckoutScreen(
                          checkoutFrom: "productDetails",
                          productName: widget.pName,
                          productQuantity: widget.pQuantity,
                          productSubtotalPrice: widget.pPrice! * widget.pQuantity,
                          productUnitPrice: widget.pPrice,
                          productTotalPrice: widget.pPrice! * widget.pQuantity,
                        ),
                      ));
                  // Navigator.push(
                  //     context,
                  //     MaterialPageRoute(
                  //       builder: (context) => CheckoutScreen(
                  //         productName: productData.productName,
                  //         productQuantity: productData.productQuantity,
                  //         productSubtotalPrice: productData.productPrice *
                  //             productData.productQuantity,
                  //         productUnitPrice: productData.productPrice,
                  //         productTotalPrice: totalPrice.toInt(),
                  //       ),
                  //     ));
                },
                child: Text("Check Out")),
            SizedBox(
              width: 30,
            ),
            ElevatedButton(
                style: ElevatedButton.styleFrom(backgroundColor: Colors.green),
                onPressed: () {
                  bool found = false;
                  setState(() {
                    box.length;
                  });
                  for (int i = 0; i < box.length; i++) {
                    ProductDetails existingProduct = box.getAt(i);
                    if (existingProduct.productName == widget.pName && existingProduct.productPrice == widget.pPrice) {
                      existingProduct.productQuantity = widget.pQuantity;
                      box.putAt(i, existingProduct);
                      found = true;
                      break;
                    }
                  }
                  if (!found) {
                    ProductDetails productDetails = ProductDetails(
                        productName: widget.pName!, productPrice: widget.pPrice!, productQuantity: widget.pQuantity, productImage: widget.pImage!);
                    box.add(productDetails);
                  }
                },
                child: Text("Add to Cart"))
          ],
        ));
  }
}
