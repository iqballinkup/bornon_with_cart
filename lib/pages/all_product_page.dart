import 'package:cart_and_hive/const/api_url.dart';
import 'package:cart_and_hive/custom_http/http_all_products.dart';
import 'package:cart_and_hive/model/api/all_products_model.dart';
import 'package:cart_and_hive/pages/product_details_page.dart';
import 'package:cart_and_hive/widgets/drawer.dart';
import 'package:cart_and_hive/widgets/shimmer_effect/shimmer_effect_6_row.dart';
import 'package:cart_and_hive/widgets/shoping_cart_badge.dart';
import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';

class AllProductPage extends StatefulWidget {
  const AllProductPage({super.key});

  @override
  State<AllProductPage> createState() => _AllProductPageState();
}

class _AllProductPageState extends State<AllProductPage> {
  late final Box box;

  @override
  void initState() {
    setState(() {});
    super.initState();
    // Get reference to an already opened box
    box = Hive.box('productBox');
  }

  double productTextSize = 14.0;
  double productPriceSize = 14.0;

  @override
  Widget build(BuildContext context) {
    var screenHeight = MediaQuery.of(context).size.height;
    var screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      // backgroundColor: Color.fromARGB(255, 226, 226, 226),
      appBar: AppBar(
        toolbarHeight: 45,
        // backgroundColor: Color.fromARGB(255, 87, 117, 133),
        title: const Text('All Products'),
        actions: [
          Padding(
            padding: const EdgeInsets.only(top: 5.0, bottom: 5.0, right: 10.0),
            child: ShoppingCartBadge(),
          ),
        ],
      ),
      drawer: MyCustomDrawer(),
      body: Padding(
        padding: const EdgeInsets.only(
            top: 5.0, left: 10.0, right: 10.0, bottom: 5.0),
        child: Column(
          children: [
            SingleChildScrollView(
              child: Container(
                height: screenHeight - 150,
                width: screenWidth,
                child: FutureBuilder(
                  future: fetchAllProducts(),
                  builder: (context, snapshot) {
                    if (snapshot.hasError) {
                      return Text('Error: ${snapshot.error}');
                    }
                    if (snapshot.hasData) {
                      List<Datum> allProducts = snapshot.data ?? [];
                      // List<Datum> allProducts = (snapshot.data as List<dynamic>)
                      //         .cast<Datum>()
                      //         .toList() ??
                      //     [];
                      return GridView.builder(
                        itemCount: allProducts.length,
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisSpacing: 5,
                            mainAxisSpacing: 10,
                            crossAxisCount:
                                MediaQuery.of(context).orientation ==
                                        Orientation.landscape
                                    ? 4
                                    : 2,
                            mainAxisExtent: 250),
                        itemBuilder: (context, index) {
                          return InkWell(
                            onTap: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (BuildContext context) {
                                return ProductDetailsPage(
                                  pImage:
                                      "$mainImageURL${allProducts[index].mainImage}",
                                  pName: allProducts[index].name!,
                                  pPrice: allProducts[index].price,
                                  pQuantity: allProducts[index].quantity,
                                  pShortDetails:
                                      allProducts[index].shortDetails!,
                                  pDescription: allProducts[index].description!,
                                  pCategoryId: allProducts[index].categoryId!,
                                );
                              })).then((_) => setState(() {}));
                            },
                            child: Card(
                                elevation: 8,
                                child: Stack(
                                  children: [
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Container(
                                          margin: EdgeInsets.only(top: 10.0),
                                          height: 200,
                                          // child: Image.asset(
                                          //   allProducts[index]["pImage"],
                                          // ),
                                          child: Image.network(
                                            "$mainImageURL${allProducts[index].mainImage}",
                                            fit: BoxFit.fill,
                                          ),
                                        ),
                                        Container(
                                          alignment: Alignment.centerLeft,
                                          margin: EdgeInsets.only(left: 10),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                "${allProducts[index].name}",
                                                style: TextStyle(
                                                    fontSize: productTextSize),
                                              ),
                                              Text(
                                                r'$' +
                                                    "${allProducts[index].price}",
                                                style: TextStyle(
                                                    fontSize: productTextSize,
                                                    color: Colors.blue,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                    Positioned(
                                        right: 10,
                                        top: 10,
                                        child: Container(
                                            height: 30,
                                            width: 30,
                                            decoration: BoxDecoration(
                                                color: Colors.redAccent,
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(20))),
                                            child: Align(
                                                alignment: Alignment.center,
                                                child: Text(
                                                  "25%",
                                                  style: TextStyle(
                                                      fontSize: 13,
                                                      color: Colors.white),
                                                ))))
                                  ],
                                )),
                          );
                        },
                      );
                    }
                    return Center(child: ShimmerEffect6Row());
                  },
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
