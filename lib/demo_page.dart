import 'package:cart_and_hive/custom_http/http_hot_deal_products.dart';
import 'package:cart_and_hive/model/api/hot_deal_model.dart';
import 'package:flutter/material.dart';

// //===============================================
// // This page is not relevant to the project, It was created to check demo designs, weather the designs work on not.
// // you may delete this file.
// //===============================================

// class DemoPage extends StatefulWidget {
//   const DemoPage({super.key});

//   @override
//   State<DemoPage> createState() => _DemoPageState();
// }

// class _DemoPageState extends State<DemoPage> {
//   double textFontSize = 13.0;
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(title: Text("Fetch Api Data")),
//       body: ExpansionTile(
//         title: Text('Shipping Address'),
//         children: [
//           // Form(
//           //   key: _formKey,
//           //   child: Column(
//           //     children: [
//           //       Container(
//           //         padding: const EdgeInsets.all(8.0),
//           //         decoration: BoxDecoration(
//           //             color: Color.fromARGB(255, 231, 231, 231),
//           //             borderRadius: BorderRadius.circular(10)),
//           //         child: Column(
//           //           children: [
//           //             Row(
//           //               children: [
//           //                 Expanded(flex: 1, child: Text("Full Name")),
//           //                 SizedBox(
//           //                   width: 5,
//           //                 ),
//           //                 Expanded(
//           //                   flex: 3,
//           //                   child: TextFormField(
//           //                     controller: _billerNameController,
//           //                     validator: (value) {
//           //                       if (value == null || value.isEmpty) {
//           //                         return 'invalid';
//           //                       }
//           //                       return null;
//           //                     },
//           //                     decoration: InputDecoration(
//           //                       hintText: "name",
//           //                       errorStyle:
//           //                           const TextStyle(fontSize: 0.01),
//           //                       contentPadding: EdgeInsets.only(
//           //                           left: 10.0,
//           //                           top: 0.0,
//           //                           bottom: 0.0,
//           //                           right: 0.0),
//           //                     ),
//           //                   ),
//           //                 ),
//           //               ],
//           //             ),
//           //             Row(
//           //               children: [
//           //                 Expanded(
//           //                     flex: 1, child: Text("Mobile No.")),
//           //                 SizedBox(
//           //                   width: 5,
//           //                 ),
//           //                 Expanded(
//           //                   flex: 3,
//           //                   child: TextFormField(
//           //                     controller: _billerMobileNoController,
//           //                     validator: (value) {
//           //                       if (value == null || value.isEmpty) {
//           //                         return "invalid";
//           //                       }
//           //                       return null;
//           //                     },
//           //                     keyboardType: TextInputType.number,
//           //                     decoration: InputDecoration(
//           //                       errorStyle: TextStyle(fontSize: 0.01),
//           //                       hintText: "mobile",
//           //                       contentPadding: EdgeInsets.only(
//           //                           left: 10.0,
//           //                           top: 0.0,
//           //                           bottom: 0.0,
//           //                           right: 0.0),
//           //                     ),
//           //                   ),
//           //                 )
//           //               ],
//           //             ),
//           //             Row(
//           //               children: [
//           //                 Expanded(flex: 1, child: Text("Address")),
//           //                 SizedBox(
//           //                   width: 5,
//           //                 ),
//           //                 Expanded(
//           //                   flex: 3,
//           //                   child: TextFormField(
//           //                     controller: _billerAddressController,
//           //                     validator: (value) {
//           //                       if (value == null || value.isEmpty) {
//           //                         return "invalid";
//           //                       }
//           //                       return null;
//           //                     },
//           //                     decoration: InputDecoration(
//           //                       hintText: "address",
//           //                       errorStyle: TextStyle(fontSize: 0.01),
//           //                       contentPadding: EdgeInsets.only(
//           //                           left: 10.0,
//           //                           top: 0.0,
//           //                           bottom: 0.0,
//           //                           right: 0.0),
//           //                     ),
//           //                   ),
//           //                 )
//           //               ],
//           //             ),
//           //             SizedBox(
//           //               height: 5,
//           //             ),
//           //             Align(
//           //                 alignment: Alignment.centerRight,
//           //                 child: ElevatedButton(
//           //                     onPressed: () {
//           //                       if (_formKey.currentState!
//           //                           .validate()) {}
//           //                     },
//           //                     child: Text("Update")))
//           //           ],
//           //         ),
//           //       ),
//           //     ],
//           //   ),
//           // ),
//           Text("data")
//         ],
//       ),
//     );
//   }
// }

class MyWidget extends StatefulWidget {
  @override
  _MyWidgetState createState() => _MyWidgetState();
}

class _MyWidgetState extends State<MyWidget> {
  // final _listViewController = ScrollController();
  // final _scrollViewController = ScrollController();

  // @override
  // void initState() {
  //   super.initState();
  //   _listViewController.addListener(_onListScroll);
  // }

  // @override
  // void dispose() {
  //   _listViewController.removeListener(_onListScroll);
  //   super.dispose();
  // }

  // void _onListScroll() {
  //   if (_listViewController.position.pixels ==
  //       _listViewController.position.maxScrollExtent) {
  //     _scrollViewController.animateTo(
  //       _scrollViewController.position.pixels + 50,
  //       duration: const Duration(milliseconds: 200),
  //       curve: Curves.linear,
  //     );
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.red,
        appBar: AppBar(
          leading: InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: Icon(Icons.menu)),
        ),
        body: SafeArea(
            child: Container(
          color: Colors.amber,
        )),
        // body: SingleChildScrollView(
        //   controller: _scrollViewController,
        //   child: Column(
        //     children: [
        //       Container(
        //         color: Colors.amber,
        //         height: 500,
        //         child: ListView.builder(
        //           controller: _listViewController,
        //           itemCount: 50,
        //           itemBuilder: (context, index) {
        //             return ListTile(title: Text('Item $index'));
        //           },
        //         ),
        //       ),
        //       SizedBox(height: 500),
        //     ],
        //   ),
        // ),
      ),
    );
  }
}
