import 'dart:async';
import 'dart:convert';
import 'package:cart_and_hive/model/api/hot_deal_model.dart';
import 'package:http/http.dart' as http;

 //=======================================
//   //Hot Deal Request
// List<Datum> hotDealProductsList = [];

// // Future<List<Datum>> delayedFetch() async {
// //   await Future.delayed(Duration(seconds: 2));
// //   return await fetchHotDealProducts();
// // }

// Future<List<Datum>> fetchHotDealProducts() async {
//   try {
//     final response =
//         await http.get(Uri.parse("https://bornonbd.com/api/hot-deal-product"));
//     Datum datum;
//     if (response.statusCode == 200) {
//       var data = jsonDecode(response.body);
//       // print(
//       //     "==========hot data products==========${data["data"]["data"][0]["name"]}");
//       // print("==========hot deal products==========${data["data"]["data"]}");
//       print("length========${data["data"]["data"].length}");

//       // for (var i in data["data"]["data"]) {
//       for (var i in data["data"]["data"]) {
//         datum = Datum.fromJson(i);
//         hotDealProductsList.add(datum);
//       }
//     } else {
//       throw Exception('Failed to load hotDealProductsList');
//     }
//   } catch (e) {
//     Future.error("=====Errrrrrrrooooorrr=====$e");
//   }
//   return hotDealProductsList;
// }
// //===============================================

//======fetch using stream controller================================

// StreamController<List<Data>> hotDealProductsController =
//     StreamController<List<Data>>();

// Future<void> fetchHotDealProducts() async {
//   try {
//     final response =
//         await http.get(Uri.parse("https://bornonbd.com/api/hot-deal-product"));
//     Data data;
//     List<Data> hotDealProductsList = [];
//     if (response.statusCode == 200) {
//       var data = jsonDecode(response.body);

//       for (var i in data["data"]["data"]) {
//         data = Data.fromJson(i);
//         hotDealProductsList.add(data);
//       }
//     } else {
//       throw Exception('Failed to load hotDealProductsList');
//     }
//     hotDealProductsController.sink.add(hotDealProductsList);
//   } catch (e) {
//     hotDealProductsController.sink.addError("=====Errrrrrrrooooorrr=====$e");
//   }
// }

// // return the StreamController
// return hotDealProductsController.stream;

// StreamController<List<Data>> hotDealProductsController =
//     StreamController<List<Data>>();

// Future<void> fetchHotDealProducts() async {
//   try {
//     final response =
//         await http.get(Uri.parse("https://bornonbd.com/api/hot-deal-product"));
//     Data data;
//     List<Data> hotDealProductsList = [];
//     if (response.statusCode == 200) {
//       var data = jsonDecode(response.body);
//       print("==========hot deal products==========${data["data"]["data"]}");
//       for (var i in data["data"]["data"]) {
//         data = Data.fromJson(i);
//         hotDealProductsList.add(data);
//       }
//     } else {
//       throw Exception('Failed to load hotDealProductsList');
//     }
//     hotDealProductsController.sink.add(hotDealProductsList);
//   } catch (e) {
//     hotDealProductsController.sink.addError("=====Errrrrrrrooooorrr=====$e");
//   }
// }

// Stream<List<Data>> getHotDealProducts() => hotDealProductsController.stream;
