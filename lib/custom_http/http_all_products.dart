import 'dart:convert';
import 'package:cart_and_hive/model/api/all_products_model.dart';
import 'package:http/http.dart' as http;

//All Products Request
List<Datum> allProductsList = [];

Future<List<Datum>> fetchAllProducts() async {
  try {
    final response =
        await http.get(Uri.parse("https://bornonbd.com/api/product"));

    Datum datum;

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      print("==========all products==========$data");
      for (var i in data["data"]) {
        datum = Datum.fromJson(i);
        allProductsList.add(datum);
      }
    }
    else {
      throw Exception('Failed to load hotDealProductsList');
    }
  }
   catch (e) {
    Future.error("=====Errrrrrrrooooorrr=====$e");
  }

  return allProductsList;
}
