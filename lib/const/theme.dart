
import 'package:flutter/material.dart';

class CustomeColor {
  static const MaterialColor cColor = const MaterialColor(
    0xff577585, // 0% comes in here, this will be color picked if no shade is selected when defining a Color property which doesn’t require a swatch.
    const <int, Color>{
      50: const Color(0xff577585), //10%
      100: const Color(0xff577585), //20%
      200: const Color(0xff577585), //30%
      300: const Color(0xff577585), //40%
      400: const Color(0xff577585), //50%
      500: const Color(0xff577585), //60%
      600: const Color(0xff577585), //70%
      700: const Color(0xff577585), //80%
      800: const Color(0xff577585), //90%
      900: const Color(0xff577585), //100%
    },
  );
}
