import 'dart:convert';

import 'package:cart_and_hive/const/api_url.dart';
import 'package:cart_and_hive/model/api/hot_deal_model.dart';
import 'package:cart_and_hive/model/hive/product.dart';
import 'package:cart_and_hive/model/product_list.dart';
import 'package:cart_and_hive/pages/product_details_page.dart';
import 'package:cart_and_hive/widgets/shimmer_effect/shimmer_effect_3_row.dart';
import 'package:cart_and_hive/widgets/shoping_cart_badge.dart';
import 'package:flutter/material.dart';
import 'package:flutter_image_slider/carousel.dart';
import 'package:hive/hive.dart';
import 'widgets/drawer.dart';
import 'dart:async';

import 'package:http/http.dart' as http;


class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  
  late final Box box;
  late Future<List<Datum>> _dataFuture;


  @override
  void initState() {
    super.initState();
    // _future = fetchHotDealProducts();
    _dataFuture = fetchHotDealProducts();
    // Get reference to an already opened box

    box = Hive.box('productBox');
  }

  //=======================================
  //Hot Deal API Request

List<Datum> hotDealProductsList = [];

// Future<List<Datum>> delayedFetch() async {
//   await Future.delayed(Duration(seconds: 2));
//   return await fetchHotDealProducts();
// }

Future<List<Datum>> fetchHotDealProducts() async {
  try {
    final response =
        await http.get(Uri.parse("https://bornonbd.com/api/hot-deal-product"));
    Datum datum;
    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      // print(
      //     "==========hot data products==========${data["data"]["data"][0]["name"]}");
      // print("==========hot deal products==========${data["data"]["data"]}");
      print("length========${data["data"]["data"].length}");

      for (var i in data["data"]["data"]) {
        datum = Datum.fromJson(i);
        hotDealProductsList.add(datum);
      }
    } else {
      throw Exception('Failed to load hotDealProductsList');
    }
  } catch (e) {
    Future.error("=====Errrrrrrrooooorrr=====$e");
  }
  return hotDealProductsList;
}
//======================================================================

  double productTextSize = 14.0;
  double productPriceSize = 14.0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 45,
        backgroundColor: Color.fromARGB(255, 87, 117, 133),
        title: const Text('Bornon'),
        actions: [
          Padding(
            padding: const EdgeInsets.only(top: 5.0, bottom: 5.0, right: 10.0),
            child: ShoppingCartBadge(),
          ),
        ],
      ),
      drawer: MyCustomDrawer(),
      backgroundColor: Color.fromARGB(255, 226, 226, 226),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 200,
              child: Carousel(
                indicatorBarColor: Colors.transparent,
                autoScrollDuration: Duration(seconds: 2),
                animationPageDuration: Duration(milliseconds: 200),
                activateIndicatorColor: Colors.black,
                animationPageCurve: Curves.linear,
                indicatorBarHeight: 20,
                indicatorHeight: 10,
                indicatorWidth: 10,
                unActivatedIndicatorColor: Colors.grey,
                stopAtEnd: false,
                autoScroll: true,
                // widgets
                items: [
                  Container(
                    color: Colors.redAccent,
                    child: Image.asset(
                      "images/slider_image1.jpg",
                      fit: BoxFit.fitWidth,
                    ),
                  ),
                  Container(
                    color: Colors.amber,
                    child: Image.asset(
                      "images/slider_image2.jpg",
                      fit: BoxFit.fitWidth,
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  // Hot Deals Products
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    padding: EdgeInsets.all(10),
                    color: Color.fromARGB(255, 46, 106, 48),
                    child: Text(
                      "Hot Deals",
                      style: TextStyle(fontSize: 12, color: Colors.white),
                    ),
                  ),
                  Container(
                    height: 180,
                    width: double.infinity,
                    margin: EdgeInsets.only(top: 10),
                    child: FutureBuilder<List<Datum>>(
                      future: _dataFuture,
                      builder: (context, snapshot) {
                        if (snapshot.connectionState == ConnectionState.done) {
                          if (snapshot.hasError) {
                            return Text('Error: ${snapshot.error}');
                          }
                          if (snapshot.hasData) {
                            List<Datum> hotDealProducts = snapshot.data ?? [];

                            return ListView.builder(
                                shrinkWrap: true,
                                scrollDirection: Axis.horizontal,
                                itemCount: hotDealProducts.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return InkWell(
                                    onTap: () {
                                      Navigator.of(context).push(
                                          MaterialPageRoute(
                                              builder: (BuildContext context) {
                                        return ProductDetailsPage(
                                          pImage:
                                              "$mainImageURL${hotDealProducts[index].mainImage}",
                                          pName:
                                              "${hotDealProducts[index].name}",
                                          pPrice: hotDealProducts[index].price,
                                          pQuantity:
                                              hotDealProducts[index].quantity!,
                                          pShortDetails: hotDealProducts[index]
                                              .shortDetails!,
                                          pDescription: hotDealProducts[index]
                                                  .description ??
                                              "No description found",
                                          pCategoryId: hotDealProducts[index]
                                              .categoryId!,
                                        );
                                      })).then((_) => setState(() {}));
                                    },
                                    child: Card(
                                      elevation: 2,
                                      child: Padding(
                                        padding: const EdgeInsets.all(12.0),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceEvenly,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            SizedBox(
                                                height: 80,
                                                width: 80,
                                                child: Image.network(
                                                  "$thumbImageURL${hotDealProducts[index].thumbImage}",
                                                )),

                                            Text(
                                              "${hotDealProducts[index].name}",
                                            ),
                                            // Text(
                                            //                 r'$' +
                                            //                     "${hotDealProducts[index].name}"
                                            //                         .toString(),
                                            //                 style: TextStyle(
                                            //                     fontSize: productTextSize,
                                            //                     color: Colors.blue,
                                            //                     fontWeight: FontWeight.bold),
                                            //               ),
                                            // Text(
                                            //   r'$' +
                                            //       categoryList[0]["pList"]
                                            //               [index]['pPrice']
                                            //           .toString(),
                                            //   style: TextStyle(
                                            //       fontSize: productTextSize,
                                            //       color: Colors.blue,
                                            //       fontWeight: FontWeight.bold),
                                            // ),
                                            Text(
                                              r'$' +
                                                  "${hotDealProducts[index].price}"
                                                      .toString(),
                                              style: TextStyle(
                                                  fontSize: productTextSize,
                                                  color: Colors.blue,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            SizedBox(
                                              width: 80,
                                              height: 25,
                                              child: ElevatedButton(
                                                  style:
                                                      ElevatedButton.styleFrom(
                                                          backgroundColor:
                                                              Colors.green),
                                                  onPressed: () {
                                                    bool found = false;
                                                    setState(() {
                                                      box.length;
                                                    });
                                                    for (int i = 0;
                                                        i < box.length;
                                                        i++) {
                                                      ProductDetails
                                                          existingProduct =
                                                          box.getAt(i);
                                                      if (existingProduct
                                                                  .productName ==
                                                              "${hotDealProducts[index].name}" &&
                                                          existingProduct
                                                                  .productPrice ==
                                                              hotDealProducts[
                                                                      index]
                                                                  .price) {
                                                        existingProduct
                                                                .productQuantity =
                                                            hotDealProducts[
                                                                    index]
                                                                .quantity!;
                                                        box.putAt(
                                                            i, existingProduct);

                                                        found = true;
                                                        break;
                                                      }
                                                    }
                                                    if (!found) {
                                                      ProductDetails
                                                          productDetails =
                                                          ProductDetails(
                                                        productName:
                                                            "${hotDealProducts[index].name}",
                                                        productPrice:
                                                            hotDealProducts[
                                                                    index]
                                                                .price!,
                                                        productQuantity:
                                                            hotDealProducts[
                                                                    index]
                                                                .quantity!,
                                                        productImage:
                                                            "$thumbImageURL${hotDealProducts[index].thumbImage}",
                                                      );
                                                      box.add(productDetails);
                                                    }
                                                  },
                                                  child: const Text(
                                                    "Add to Cart",
                                                    style:
                                                        TextStyle(fontSize: 9),
                                                  )),
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                  );
                                });
                          }
                        }

                        return ShimmerEffect3Row();
                      },
                    ),
                  ),

                  //Women Products
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    padding: EdgeInsets.all(10),
                    color: Color.fromARGB(255, 46, 106, 48),
                    child: Text(
                      "Women Products",
                      style: TextStyle(fontSize: 12, color: Colors.white),
                    ),
                  ),
                  Container(
                    height: 180,
                    width: double.infinity,
                    margin: EdgeInsets.only(top: 10),
                    child: ListView.builder(
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        itemCount: categoryList[1]["pList"].length,
                        itemBuilder: (BuildContext context, int index) {
                          return InkWell(
                            onTap: () {
                              // Navigator.of(context).push(MaterialPageRoute(
                              //     builder: (BuildContext context) {
                              //   return ProductDetailsPage(
                              //     pImage: categoryList[1]["pList"][index]
                              //         ["pImage"],
                              //     pName: categoryList[1]["pList"][index]
                              //         ["pName"],
                              //     pPrice: categoryList[1]["pList"][index]
                              //         ['pPrice'],
                              //     pQuantity: categoryList[1]["pList"][index]
                              //         ["pQuantity"],
                              //   );
                              // })).then((_) => setState(() {}));
                            },
                            child: Card(
                              elevation: 2,
                              child: Padding(
                                padding: const EdgeInsets.all(12.0),
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    SizedBox(
                                      height: 80,
                                      width: 80,
                                      child: Image.asset(categoryList[1]
                                          ["pList"][index]["pImage"]),
                                    ),
                                    Text(
                                      categoryList[1]["pList"][index]["pName"]
                                          .toString(),
                                      style:
                                          TextStyle(fontSize: productTextSize),
                                    ),
                                    Text(
                                      r'$' +
                                          categoryList[1]["pList"][index]
                                                  ['pPrice']
                                              .toString(),
                                      style: TextStyle(
                                          fontSize: productTextSize,
                                          color: Colors.blue,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    SizedBox(
                                      width: 80,
                                      height: 25,
                                      child: ElevatedButton(
                                          style: ElevatedButton.styleFrom(
                                              backgroundColor: Colors.green),
                                          onPressed: () {
                                            bool found = false;
                                            setState(() {
                                              box.length;
                                            });
                                            for (int i = 0;
                                                i < box.length;
                                                i++) {
                                              ProductDetails existingProduct =
                                                  box.getAt(i);
                                              if (existingProduct.productName ==
                                                      categoryList[1]["pList"]
                                                          [index]["pName"] &&
                                                  existingProduct
                                                          .productPrice ==
                                                      categoryList[1]["pList"]
                                                          [index]['pPrice']) {
                                                existingProduct
                                                        .productQuantity =
                                                    categoryList[1]["pList"]
                                                        [index]['pQuantity'];
                                                box.putAt(i, existingProduct);

                                                found = true;
                                                break;
                                              }
                                            }
                                            if (!found) {
                                              ProductDetails productDetails =
                                                  ProductDetails(
                                                      productName: categoryList[1]
                                                              ["pList"][index]
                                                          ["pName"],
                                                      productPrice:
                                                          categoryList[1]["pList"]
                                                              [index]['pPrice'],
                                                      productQuantity:
                                                          categoryList[1]
                                                                      ["pList"]
                                                                  [index]
                                                              ['pQuantity'],
                                                      productImage:
                                                          categoryList[1]
                                                                  ["pList"]
                                                              [index]["pImage"]);
                                              box.add(productDetails);
                                            }
                                          },
                                          child: const Text(
                                            "Add to Cart",
                                            style: TextStyle(fontSize: 9),
                                          )),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          );
                        }),
                  ),

                  //Men Products

                  Container(
                    margin: EdgeInsets.only(top: 10),
                    padding: EdgeInsets.all(10),
                    color: Color.fromARGB(255, 46, 106, 48),
                    child: Text(
                      "Men Products",
                      style: TextStyle(fontSize: 12, color: Colors.white),
                    ),
                  ),
                  Container(
                    height: 180,
                    width: double.infinity,
                    margin: EdgeInsets.only(top: 10),
                    child: ListView.builder(
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        itemCount: categoryList[2]["pList"].length,
                        itemBuilder: (BuildContext context, int index) {
                          return InkWell(
                            onTap: () {
                              // Navigator.of(context).push(MaterialPageRoute(
                              //     builder: (BuildContext context) {
                              //   return ProductDetailsPage(
                              //     pImage: categoryList[2]["pList"][index]
                              //         ["pImage"],
                              //     pName: categoryList[2]["pList"][index]
                              //         ["pName"],
                              //     pPrice: categoryList[2]["pList"][index]
                              //         ['pPrice'],
                              //     pQuantity: categoryList[2]["pList"][index]
                              //         ["pQuantity"],
                              //   );
                              // })).then((_) => setState(() {}));
                            },
                            child: Card(
                              elevation: 2,
                              child: Padding(
                                padding: const EdgeInsets.all(12.0),
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    SizedBox(
                                      height: 80,
                                      width: 80,
                                      child: Image.asset(categoryList[2]
                                          ["pList"][index]["pImage"]),
                                    ),
                                    Text(
                                      categoryList[2]["pList"][index]["pName"]
                                          .toString(),
                                      style:
                                          TextStyle(fontSize: productTextSize),
                                    ),
                                    Text(
                                      r'$' +
                                          categoryList[2]["pList"][index]
                                                  ['pPrice']
                                              .toString(),
                                      style: TextStyle(
                                          fontSize: productTextSize,
                                          color: Colors.blue,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    SizedBox(
                                      width: 80,
                                      height: 25,
                                      child: ElevatedButton(
                                          style: ElevatedButton.styleFrom(
                                              backgroundColor: Colors.green),
                                          onPressed: () {
                                            bool found = false;
                                            setState(() {
                                              box.length;
                                            });
                                            for (int i = 0;
                                                i < box.length;
                                                i++) {
                                              ProductDetails existingProduct =
                                                  box.getAt(i);
                                              if (existingProduct.productName ==
                                                      categoryList[2]["pList"]
                                                          [index]["pName"] &&
                                                  existingProduct
                                                          .productPrice ==
                                                      categoryList[2]["pList"]
                                                          [index]['pPrice']) {
                                                existingProduct
                                                        .productQuantity =
                                                    categoryList[2]["pList"]
                                                        [index]['pQuantity'];
                                                box.putAt(i, existingProduct);

                                                found = true;
                                                break;
                                              }
                                            }
                                            if (!found) {
                                              ProductDetails productDetails =
                                                  ProductDetails(
                                                      productName: categoryList[2]
                                                              ["pList"][index]
                                                          ["pName"],
                                                      productPrice:
                                                          categoryList[2]["pList"]
                                                              [index]['pPrice'],
                                                      productQuantity:
                                                          categoryList[2]
                                                                      ["pList"]
                                                                  [index]
                                                              ['pQuantity'],
                                                      productImage:
                                                          categoryList[2]
                                                                  ["pList"]
                                                              [index]["pImage"]);
                                              box.add(productDetails);
                                            }
                                          },
                                          child: const Text(
                                            "Add to Cart",
                                            style: TextStyle(fontSize: 9),
                                          )),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          );
                        }),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
